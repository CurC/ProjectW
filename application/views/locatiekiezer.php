<div id="hoofdtekst">
    <div id="map" style="width: 500px; height: 500px; display: block; margin-left: auto; margin-right: auto">
    </div>
	<script>
        function initMap(){
            var map;
            var marker;
            var InitLoc = {lat:parseFloat('<?php echo $InitLat; ?>'), 
                            lng:parseFloat('<?php echo $InitLong; ?>')};
            var Loc = {lat:0, lng:0};
            var geocoder = new google.maps.Geocoder();
            map = new google.maps.Map(document.getElementById('map'),{
                center: InitLoc,
                zoom: 18,
                mapTypeId: google.maps.MapTypeId.ROADMAP
            });

            marker = new google.maps.Marker({
                position: InitLoc,
                map: map,
                draggable: true,
            });

            var input = document.getElementById('map-input');
            var autocomplete = new google.maps.places.Autocomplete(
                (document.getElementById('map-input'))
                );
            autocomplete.bindTo('bounds', map);

            autocomplete.addListener('place_changed', function(){
                marker.setVisible(false);
                var place = autocomplete.getPlace();
				
                if (!place.geometry){
                    return;
                }
                if (place.geometry.viewport){
                    map.fitBounds(place.geometry.viewport);
                } 
				else{
                    map.setCenter(place.geometry.location);
                    map.setZoom(18);
                }
				
                marker.setPosition(place.geometry.location);
                marker.setVisible(true);
                document.getElementsByName('mapValue')[0].value = document.getElementById('map-input').value;
                document.getElementsByName('mapValueCoor')[0].value = marker.getPosition();
                    
            });

            map.addListener('click', function(event){
                //geocode_lookup('latlng', event.latLng);
                marker.setPosition(event.latLng);
                geocode_lookup('latLng', marker.getPosition());
            });

            marker.addListener('drag', function(){
                //geocode_lookup('latLng', marker.getPosition() );
                document.getElementById('map-input').value = marker.getPosition();
            });

            marker.addListener('dragend', function(){
                 geocode_lookup('latLng', marker.getPosition());
            });

       		function geocode_lookup( type, value, update ){
                // default value: update = false
                update = typeof update !== 'undefined' ? update : false;

                request = {};
                request[type] = value;

                geocoder.geocode(request, function(result, status){
                    document.getElementById('map-input').value = result[0].formatted_address;
                    document.getElementsByName('mapValue')[0].value = document.getElementById('map-input').value;
                    document.getElementsByName('mapValueCoor')[0].value = marker.getPosition();
                    marker.setPlace(result[0].place_id);
                });
            }
            map.controls[google.maps.ControlPosition.TOP_CENTER].push(input);
        }
		
    </script>
    
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyATFNWg7BFS4338FgDzFPkliiNhDfd0T3U&libraries=places&callback=initMap" async defer></script>
    
    <div id="map-controls">
    
        <form method="post" action="<?php echo base_url('index.php/Location/submit'); ?>">
        	<input type="hidden" name="mapValue">
        	<input type="hidden" name="mapValueCoor">
        	<input type="text" id="map-input" value="<?php echo $_SESSION['eLatLng'];?>" 
            placeholder="Search Box <?php echo $_SESSION["eLatLngErr"];?>" 
            style="margin-top: 10px; height: 22px; outline: none; width: 250px">
        	<input class="knop_locatiekiezer" id="verzend_knop" type="submit" value="Voltooien">
        </form>
        
    </div>
    
    <form class="knop_locatiekiezer" action="https://www.projects.science.uu.nl/INFOB1PICA/2015/02/www/index.php/Events/mainEvent">
        <input type="submit" class="verzendknop" value="Terug">
    </form>

</div>
