<div id="hoofdtekst">
	<div id="kalender-header-text">
		<h1 id = "tester">
			<?php echo "Prik je datum voor: " . $_SESSION["CurrentEventName"]; ?>
		</h1>
	</div>

	<div id="kalender">
		<!-- Genereert de kalender van de controller Kalender.php -->
		<?php
			echo $kalender;
		?>
	</div>
	
	<?php
		//Haalt de dag op van de gekozen data uit de database
		//Zet hem ook in een bruikbaar formaat
		if (isset($data1)){
			$data1Arr = (explode(' ', $data1));
			$dagdata1 = $data1Arr[0];
			$maandJaardata1 = $data1Arr[1] . " " . $data1Arr[2];
			
		}
		
		if (isset($data2)){
			$data2Arr = (explode(' ', $data2));
			$dagdata2 = $data2Arr[0];
			$maandJaardata2 = $data2Arr[1] . " " . $data2Arr[2];
		}
		
		if (isset($data3)){
			$data3Arr = (explode(' ', $data3));
			$dagdata3= $data3Arr[0];
			$maandJaardata3 = $data3Arr[1] . " " . $data3Arr[2];
		}
		
		if (isset($data4)){
			$data4Arr = (explode(' ', $data4));
			$dagdata4 = $data4Arr[0];
			$maandJaardata4 = $data4Arr[1] . " " . $data4Arr[2];
		}	
	?>
	
	<script>
		/* Zet de datum in een goed formaat voor php en stuurt hem op naar de controller */
		window.onload = selectData();
		function datum(sent){
			var months = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
			var a = sent.innerHTML;
			var b = document.getElementById('kalender-maand').textContent;
			var c = a + " " + b;
			var d = c.replace(/\s+/g, '-');
			console.log(d);
			window.location.assign("https://www.projects.science.uu.nl/INFOB1PICA/2015/02/www/index.php/Kalender/loadSession/" + d);
		}
		
		//De style van de blokjes die zijn gekozen in de database worden hier gecolorcoded
		function selectData(){
			var collectie = document.getElementsByClassName('kalender-cell-link');
			var collectieHuidig = document.getElementsByClassName('highlight');
			var headerMJ = document.getElementById('kalender-maand').textContent;
			var headerMJ_r = headerMJ.replace(/\s/g,' ');
		
			var data1 = "<?php if(isset($dagdata1))echo $dagdata1; ?>";
			var data1MJ = "<?php if(isset($maandJaardata1))echo $maandJaardata1; ?>";

			var data2 = "<?php if(isset($dagdata2))echo $dagdata2; ?>";
			var data2MJ = "<?php if(isset($maandJaardata2))echo $maandJaardata2; ?>";
		
			var data3 = "<?php if(isset($dagdata3))echo $dagdata3; ?>";
			var data3MJ = "<?php if(isset($maandJaardata3))echo $maandJaardata3; ?>";
		
			var data4 = "<?php if(isset($dagdata4))echo $dagdata4; ?>";
			var data4MJ = "<?php if(isset($maandJaardata4))echo $maandJaardata4; ?>";
			console.log(pad(collectie[8].textContent));
		
			for(var i = 0; i < collectie.length;i++){
				var cellS = collectie[i];
				if (pad(cellS.innerHTML) == data1 && headerMJ_r == data1MJ){
					cellS.parentElement.style.backgroundColor = '#FF5465';
					cellS.parentElement.style.border = '1px solid black';
				}
				else if (pad(cellS.innerHTML) == data2 && headerMJ_r == data2MJ){
					cellS.parentElement.style.backgroundColor = '#5476FF';
					cellS.parentElement.style.border = '1px solid black';
				}
				else if (pad(cellS.innerHTML) == data3 && headerMJ_r == data3MJ){
					cellS.parentElement.style.backgroundColor = '#7AB85C';
					cellS.parentElement.style.border = '1px solid black';
				}
				else if (pad(cellS.innerHTML) == data4 && headerMJ_r == data4MJ){
					cellS.parentElement.style.backgroundColor = '#9D4E89';
					cellS.parentElement.style.border = '1px solid black';
				}
			}
		
			if (collectieHuidig[0].innerHTML == data1){
				collectieHuidig[0].parentElement.style.backgroundColor = '#FF5465';
				collectieHuidig[0].parentElement.style.border = '1px solid black';
			}
			else if (collectieHuidig[0].innerHTML == data2){
				collectieHuidig[0].parentElement.style.backgroundColor = '#5476FF';
				collectieHuidig[0].parentElement.style.border = '1px solid black';
			}
			else if (collectieHuidig[0].innerHTML == data3){
				collectieHuidig[0].parentElement.style.backgroundColor = '#7AB85C';
				collectieHuidig[0].parentElement.style.border = '1px solid black';
			}
			else if (collectieHuidig[0].innerHTML == data4){
				collectieHuidig[0].parentElement.style.backgroundColor = '#9D4E89';
				collectieHuidig[0].parentElement.style.border = '1px solid black';
			}
		}
		//Helpt bij het correct selecteren van de vakjes
		function pad(n) {
			var a = parseInt(n);
			if (a < 10){
				n = "0" + n;
			}
			return n;
		}
		
	</script>
    
	<div id='datas'>
		<ul id = 'datas-text'>
        
			<li>
				<span class = "datums" id="datum1">Voorkeursdatum: </span>
				<?php 
					if (isset($data1)){
						echo "- " . $data1 . "<br/>";
					}
					else{
						echo"Zonder deze te kiezen kan je toch geen evenement organiseren?";
					} 
				?>
			</li>
            
			<li>
				<span class = "datums" id ="datum2">Tweede keuze: </span>
				<?php
					if (isset($data2)){
						echo "- " . $data2 . "<br/>";
					}
					else{
						echo "Nog niet gekozen";
					} 
				?>
			</li>
            
			<li>
				<span class = "datums" id = "datum3">Derde keuze: </span>
				<?php
					if (isset($data3)){
						echo "- " . $data3 . "<br/>";
					}
					else{
						echo "Deze is nog niet gekozen";
					} 
				?>
			</li>
            
			<li>
				<span class = "datums" id = "datum4">Voor als er echt niks anders op zit: </span>
				<?php
					if (isset($data4)){
						echo "- " . $data4 . "<br/>";
					}
					else{
						echo "Nog te kiezen voor een noodgeval";
					} 
				?>
			</li>
            
		</ul>
    
		<?php 
			echo $Open_info; 
		?>

	</div>
    
	<form action="https://www.projects.science.uu.nl/INFOB1PICA/2015/02/www/index.php/Events/mainEvent">
    	<input type="submit" class="verzendknop" value="Terug">
    </form>
    
    <form action="https://www.projects.science.uu.nl/INFOB1PICA/2015/02/www/index.php/Kalender/resetDates">
		<input type="submit" class="verzendknop" value="Verwijder de Data">
	</form>
	
    <form class="knoppen_datum" action="https://www.projects.science.uu.nl/INFOB1PICA/2015/02/www/index.php/Kalender/voltooiDate"> 
    	<input type="submit" class="verzendknop" value="Voltooien" />   
    </form>

</div>
