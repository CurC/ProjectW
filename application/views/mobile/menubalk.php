<body>
    <div id="content">
		<div id="menubalk">
			<ul class="menu_lijst">
            
    			<li class="menu_lijst_blok_links">
                	<a href="<?php echo base_url('index.php/Main/events');?>">
                		<img class="menu_picture" src="<?php echo base_url('images/event.png');?>"/>
                    </a>
                </li>
    			
                <li class="menu_lijst_blok">
                	<a href="<?php echo base_url('index.php/Events/nieuw');?>">
                		<img class="menu_picture" src="<?php echo base_url('images/nieuw.png');?>" />
                    </a>
                </li>
    			
                <li class="menu_lijst_blok">
                	<a href="<?php echo base_url('index.php/Main/index'); ?>">
                    	<img class="menu_picture" src="<?php echo base_url('images/logo_big.png');?>"/>
                    </a>
                </li>
    			
                <li class="menu_lijst_blok">
                	<a href="<?php echo base_url('index.php/Main/info'); ?>">
                    	<img class="menu_picture" src="<?php echo base_url('images/info.png');?>"/>
                    </a>
                </li>
                
    			<li class="menu_lijst_blok_rechts">
                	<div id="dropdown_wrap">
                    	<a class="inhoud_dropdown_block" href="
						<?php 
        					if ($_SESSION["ingelogd"] == true){
            						echo base_url('index.php/Login/account');
        						} 
								else{
            						echo base_url('index.php/Login');
        						}
						?>">
        					<img class="dropdown_picture" src="
							<?php 
        						if ($_SESSION["ingelogd"] == true){
            						echo base_url('images/account.png');
        						} 
								else {
            					echo base_url('images/inloggen.png');
        						}
							?>"/>
                        </a>
					</div>
   				</li>
			</ul>
		</div>