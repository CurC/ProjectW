<div id="hoofdtekst" >
    <h1>Log in</h1>
	<form method="post" action="<?php echo base_url('index.php/Login/checklogin'); ?>">
    	<p>
        	<input class="inputbox" maxlength="80" type="email" name="login" value="<?php echo $email;?>" placeholder="Email">
            <div class="error_message">
				<?php echo $emailErr;?>
            </div>
        </p>
        
    	<p>
        	<input class="inputbox" maxlength="80" type="password" name="password" placeholder="Password">
            <div class="error_message">
				<?php echo $passErr;?>
				<?php echo $activatieErr;?>
            </div>
        </p>
        
    	<p class="remember_me">
    		<label>
        		<input type="checkbox" name="remember_me" id="remember_me" value="on">
                Onthoud mij op dit apparaat<br> (U geeft toestemming voor cookies)
    		</label>
    	</p>
    	<div class="login_knoppen">
    		<p class="submit">
            	<input type="submit" class="verzendknop" name="commit" value="Login">
    		</p>
        </div>         
    </form>
    
    <form action="<?php echo base_url('index.php/Login/veranderpass'); ?>">
    	<input type="submit" class="verzendknop" value="Wachtwoord vergeten?">
    </form>
    
    <div class="login_knoppen">
    	<form style="margin-bottom:20px;" action="<?php echo base_url('index.php/Login/registratie'); ?>">
    		<input type="submit" class="verzendknop" value="Registreren">
    	</form>
    </div>
</div>