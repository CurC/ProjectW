<div id="hoofdtekst">
    <h1>Registreren</h1>
    
    <form method="post" action="<?php echo base_url('index.php/Login/nieuwAcc'); ?>">
        <p>Email adres: 
        	<input maxlength="80" type="email" name="email" value="<?php echo $email;?>" placeholder="Email">
            	<div class="error_message">
					<?php echo $emailErr;?>
                </div>
        </p>
        
        <p>Wachtwoord: 
        	<input maxlength="80" type="password" name="password" placeholder="Password">
            	<div class="error_message">
					<?php echo $passErr;?>
                </div>
        </p>
        
        <p>Geboortedatum: 
        	<input style="width:220px;" type="date" name="geboortedatum" value="<?php echo date('Y-m-d'); ?>">
        </p>
        
        <p>Voornaam: 
        	<input maxlength="80" type="text" name="voornaam" value="<?php echo $vnaam;?>" placeholder="Voornaam">
            	<div class="error_message">
					<?php echo $vnaamErr;?>
                </div>
        </p>
        
        <p>Achternaam: 
        	<input maxlength="80" type="text" name="achternaam" value="<?php echo $anaam;?>" placeholder="Achternaam">
            	<div class="error_message">
					<?php echo $anaamErr;?>
                </div>
        </p>
        
        <p>Geslacht: 
        	<div class="geslacht">
            	<input type="radio" name="geslacht" value="M" placeholder="Man" checked="checked">Man
        		<input style="margin-left:80px;" type="radio" name="geslacht" value="V" placeholder="Vrouw">Vrouw
            </div>
        </p>
        
        <p>
        	<input class="verzendknop" onclick="alert('Er is een activatiemail verstuurt naar uw ingevoerde e-mailadres. U kunt inloggen nadat u op de link in de mail heeft geklikt. &#40;
            Check eventueel uw spam folder &#41;')" type="submit">
        </p>
        
    </form>
</div>