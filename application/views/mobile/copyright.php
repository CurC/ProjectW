<div id="hoofdtekst">
	<h1>Copyright</h1>
	<p>Bron en license van plaatje van Open event knop op Main/events:
    
    	<ul>
        
        	<li>
    			<a style="font-size:25px;" href="https://www.flickr.com/photos/52170719@N08/7129476615">Source</a>
            </li>
            
        	<li>
    			<a style="font-size:25px;" href="https://creativecommons.org/licenses/by-sa/2.0/">License</a>
            </li>
            
		</ul>
        
	</p>
</div>