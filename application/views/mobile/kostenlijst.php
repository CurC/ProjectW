<div id="hoofdtekst">
	<p>
		<h1>Kostenlijst</h1>
    	<?php 
    		echo $costs;
    	?>
    
    	<p>Totaal: &euro;<?php echo $totaal ?></p>
        <form class="gasten_form" method="post" action="https://www.projects.science.uu.nl/INFOB1PICA/2015/02/www/index.php/Costlist/toevoegen">
        	<div class="layout_gasten">
               
            	<div class="textfield_gasten">
                	<p>Kostenbeschrijving:
                  		<input class="inputbox" maxlength="140" type="text" name="naam" value="<?php echo $_SESSION["kostNaam"];?>" placeholder="Kostenbeschrijving">
                        
                        <div class="error_message">
							<?php echo $_SESSION["kostNaamErr"];?>
                    	</div>
                            
                    </p>
                </div>
                    
                <div>
                	<p>Prijs van de kosten in €0.00:
                		<input class="inputbox" type="text" name="prijs" onkeypress="return isNumberKey(event)" value="<?php echo $_SESSION["prijs"];?>" placeholder="0.00">
                            
                        <div class="error_message">
							<?php echo $_SESSION["prijsErr"];?>
                        </div>
                            
                	</p>
            	</div>
                    
            </div>
            
            <input type="submit" class="verzendknop" value="Toevoegen">      
        </form>
            
        <form class="knoppen_gasten" action="https://www.projects.science.uu.nl/INFOB1PICA/2015/02/www/index.php/Costlist/voltooid"> 
            <input type="submit" class="verzendknop" value="Voltooien" />   
        </form>
                    
        <form class="knoppen_gasten" action="https://www.projects.science.uu.nl/INFOB1PICA/2015/02/www/index.php/Events/mainEvent">
            <input type="submit" class="verzendknop" value="Terug">
        </form>
	</p>
</div>

<script type="text/javascript">

	function isNumberKey(evt){
    	var charCode = (evt.which) ? evt.which : evt.keyCode;
		
    	if (charCode != 46 && charCode > 31 && (charCode < 48 || charCode > 57)){
        	return false;
		}
		return true;
	}
	
	function verwijderKosten(kost_nummer){
    	var b = kost_nummer;
		var a = document.querySelectorAll('.cost_remove');

   		if(confirm('Weet je zeker dat u deze kosten wilt verwijderen?')   == true){
    		a[b].submit();
    	}
	}
	
</script>