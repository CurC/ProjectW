<div id="hoofdtekst">
	<h1>Een evenementenplanner maken, dat is pas een feest!</h1>
    
    <ul class="menublok menublok_review">
    	
        <li class="menublokken menublokken_review">
        	<p id="reviewText">Ik zit dit hier nu te typen, al denkende aan alle uren die iedereen hierin heeft gestoken. Een geweldig project, al zeg ik het zelf. Veel werk, veel denken, maar
            ook veel lol. Natuurlijk ontstaat zoiets als dit niet vanzelf en dat merk je wel aan de uren die we erin hebben moeten steken. Zelf heb ik zeker veel genoten van alle dingen die we
            hebben gedaan, langzaam de site zien groeien, bloeien en uiteindelijk zo'n project zoals dit af te kunnen leveren. Wauw.</p>
        </li>
        
        <li class="menublokken menublokken_foto_review">
        	<img class="review_picture review_picture1" src="<?php echo base_url('images/typing.jpg');?>"/>
        </li>  
         
	</ul>
    
    <ul class="menublok menublok_review">
    
        <li class="menublokken menublokken_foto_review">
        	<img class="review_picture review_picture2" src="<?php echo base_url('images/Universiteit_Utrecht.jpg');?>"/>
        </li>   
		
        <li class="menublokken menublokken_review">
        	<p style="padding-right:20px;" id="reviewText">Het begon allemaal op de Universiteit van Utrecht. Samen in een kamertje, peinzend over wat ons project wel niet kon worden. De wildste ideeën zijn
            rondgeslingerd. Van games tot leerapps, van Spotify-en-chill tot windparkbeheer. Zoveel ideeën, maar uiteindelijk kwamen we overeen dat een evenementenplanner wel echt ons ding is.
            Na wat wilde schetsen van hoe het project tot leven zou komen, begonnen we als een stelletje wilden weg te tikken, regel na regel code voortvloeiend uit onze vingers. Urenlang
            samen op Skype, urenlang denken, proberen, verwijderen en opnieuw proberen. Het leek nooit te stoppen. Maar toen, eindelijk, het resultaat. Project W.</p>
        </li>
    
    </ul>
    
    <ul class="menublok menublok_review">
    	
        <li class="menublokken menublokken_review">
        	<p id="reviewText">Het feest wat we daarna hielden, was de naam Project W waardig. Ruim 15.000 vatten bier zijn er doorheen gegaan, heel Utrecht had een kater. Politie van over het
            hele land was ingeschakeld, top-DJ's van over de hele wereld zijn ingevlogen en iedere kamer in Utrecht had minstens 10 feestbeesten erin, tot aan de wc's toe. Dankzij deze
            geweldige evenementenplanner is dit gigantische feest zonder ook maar enig probleem geregeld. Geen gewonden, arrestaties, geen onverwachte kosten of ongewenste gasten. Een feest
            waar je zeker weten U tegen zegt.</p>
        </li>
        
        <li class="menublokken menublokken_foto_review">
        	<img class="review_picture review_picture3" src="<?php echo base_url('images/party.jpg');?>"/>
        </li>
        
	</ul>
    
    <h1 style="text-transform: none;">Dubbel U.</h1>
    
    <img class="review_projectW" src="<?php echo base_url('images/logo_big.png');?>"/>
</div>