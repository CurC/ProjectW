<div id="hoofdtekst">
	<div class="home_parent">
       
       	<div class="menubalk">
        
    		<a class="menublok menublok_homepage" id="linksboven" href="<?php echo base_url('index.php/Main/info'); ?>">
            	<div class="menudetails">
                	Hier vind je nuttige info!
                </div>
            </a>
            
        </div>    
            
        <div class="menubalk"> 
           
    		<a class="menublok menublok_homepage" id="rechtsboven" href="<?php echo base_url('index.php/Main/all_events'); ?>">
            	<div class="menudetails">
                	Alle events op een rijtje!
                </div>
            </a>
            
        </div>
        
        <div class="menubalk">
        
        	<a class="menublok menublok_homepage" id="linksonder" href="<?php echo base_url('index.php/Main/review'); ?>">
            	<div class="menudetails">
                	"Zo'n party zie je niet vaak!"
                </div>
            </a>
            
        </div>    
            
        <div class="menubalk">
            
        	<a class="menublok menublok_homepage" id="rechtsmidden" href="<?php echo base_url('index.php/Main/info#anchor'); ?>">
            	<div class="menudetails">
                	Het brein achter Project W!
                </div>
            </a>
            
        </div>
           
    </div>
</div>