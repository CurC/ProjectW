<div id="hoofdtekst">
    <div class="events">
    	<a class="layout_events" id="nieuw_event_knop" href="<?php echo base_url('index.php/Events/nieuw'); ?>">
        	<p class="layout_events_tekst">Nieuw event</p>
        </a>
        
    	<a class="layout_events" id="mijn_events_knop" href="<?php echo base_url('index.php/Events/overzicht'); ?>">
        	<p class="layout_events_tekst">Mijn events</p>
        </a> 
        
        <a class="layout_events" id="events_uitgenodigd_knop" href="<?php echo base_url('index.php/Events/uitgenodigd_voor'); ?>">
        	<p class="layout_events_tekst">Uitnodigingen</p>
        </a>
         
        <a class="layout_events" id="open_events_knop" href="<?php echo base_url('index.php/Main/all_events'); ?>">
        	<p class="layout_events_tekst">Open events</p>
        </a> 
    </div>
</div>