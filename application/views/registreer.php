<div id="hoofdtekst">
    <h1>Registreren</h1>
    
    <form class="registreer_form" method="post" onsubmit="alert('Er is een activatiemail verstuurd naar uw ingevoerde e-mailadres. U kunt inloggen nadat u op de link in de mail heeft geklikt. &#40; Check eventueel uw spam folder &#41;')" action="<?php echo base_url('index.php/Login/nieuwAcc'); ?>">
        
        <p class="layout_text">Email adres:</p>
        	<input maxlength="80" type="email" name="email" value="<?php echo $email;?>" placeholder="Email">
            	<div class="error_message">
					<?php echo $emailErr;?>
                </div>
        
        <p>Wachtwoord:</p>
        	<input maxlength="80" type="password" name="password" placeholder="Password">
            	<div class="error_message">
					<?php echo $passwErr;?>
                </div>
        
        <p>Wachtwoord controleren:</p>
        	<input maxlength="80" type="password" placeholder="Password" name="password2">
            	<div class="error_message">
					<?php echo $passwcheckErr;?>
                </div>
        
        <p>Geboortedatum:</p> 
        	<input type="date" name="geboortedatum" value="<?php echo date('Y-m-d'); ?>">
        
        <p>Voornaam:</p> 
        	<input maxlength="80" type="text" name="voornaam" value="<?php echo $vnaam;?>" placeholder="Voornaam">
            	<div class="error_message">
					<?php echo $vnaamErr;?>
                </div>
        
        
        <p>Achternaam:</p> 
        	<input maxlength="80" type="text" name="achternaam" value="<?php echo $anaam;?>" placeholder="Achternaam">
            	<div class="error_message">
					<?php echo $anaamErr;?>
                </div>
        
        
        <p>Geslacht:</p> 
        	<div class="geslacht">
            	<input type="radio" name="geslacht" value="M" placeholder="Man" checked="checked">Man
        		<input type="radio" name="geslacht" value="V" placeholder="Vrouw">Vrouw
            </div>
        
        	<input type="submit" class="verzendknop" id="verzendknop_registreer" value="Registreer">        
    </form>
</div>