<div id="hoofdtekst">
    <?php echo '<h1>' . $EventNaam . '</h1>' ?>
    
    <ul class="menublok menublok_landing">
    
    	<li class= "menublokken menublokken_header" id= "beschrijving_header">
        	<h1 class="stap_2_h">Beschrijving:</h1>
        </li>   
        
        <li class= "menublokken menublokken_tekst">
        	<p>
				<?php 	if($beschrijving == null){
							echo '<div class="error_message">Geef een beschrijving aan je event!</div>';
						}
    					else {
							echo wordwrap($beschrijving, 120 ,"<br>\n", true);
						}
				?>
        </li>
	
    </ul>
    
	<ul class="menublok menublok_landing">
    
    	<li class= "menublokken menublokken_header">
        	<h1 class="stap_1_h">Locatie:</h1>
        </li>  
         
        <li class= "menublokken menublokken_tekst">
        	<p>
        		<?php 	if($locatie == null){
							echo '<div class="error_message">Er is geen locatie gekozen!</div>';
						}
    					else {
							echo $locatie;
						} 
				?>
            </p>
        </li>
        
	</ul>
    
	<ul class="menublok menublok_landing">
    	
        <li class= "menublokken menublokken_header">
        	<h1 class="stap_1_h">Gasten:</h1>
        </li>   
        
        <li class= "menublokken menublokken_gasten">
        	<p>
		 		<?php 	if($gast == null){ 
							echo '<p class="error_message">Er zijn nog geen gasten</p>';
						}
  						else {
							echo $gast;
						} 
				?>
            
            	<form action="https://www.projects.science.uu.nl/INFOB1PICA/2015/02/www/index.php/Events/gaNaarOpenEvent">
        			<input type="submit" class="verzendknop" value="Deelnemen">
    			</form>
        	</p>
    	</li>
        
	</ul>
        
	<form class="verzend_mainevent" action="https://www.projects.science.uu.nl/INFOB1PICA/2015/02/www/index.php/Main/all_events">
        <input type="submit" class="verzendknop" value="Terug naar overzicht">
    </form>
</div>