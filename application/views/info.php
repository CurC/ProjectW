<div id="hoofdtekst">
    <div class="IE_info_fix">
    <div class="IE_info_fix2">
	<h1 id="titel_info">Welkom op Project W, dé site om je evenement mee te plannen!</h1>
    </div>
    <div class="IE_info_fix2">
	<p>Op Project W kan je met gemak alle details van je evenement plannen, van een klein feestje, tot een mega-event voor duizenden mensen. Om jouw eigen evenement te starten, hoef je 
    alleen maar een eigen account aan te maken, en de details in te vullen.</p>
    </div>
    </div>
	<ul class="menublok menublok_info">
    
    	<li class="menublokken menublokken_foto">
        	<a class="menudetails" href="https://www.projects.science.uu.nl/INFOB1PICA/2015/02/www/images/Stap_1.PNG">
            	<img class="image_details" src="https://www.projects.science.uu.nl/INFOB1PICA/2015/02/www/images/Stap_1.PNG"/>
            </a>
        </li>
           
        <li class="menublokken menublokken_info">
        	<h1 class="stap_1_h">Stap 1</h1>
        	<p>Log in met je account, of maak een nieuw account aan. Hierbij kan je nog kiezen om je gegevens te onthouden. Handig h&#232;?</p>
        </li>
        
	</ul>
    
    <ul class="menublok menublok_info">
    
    	<li class="menublokken menublokken_info">
        	<h1 class="stap_1_h">Stap 2</h1>
        	<p>Kies "Nieuw Event" als je een compleet nieuw event wil maken, of kies "Mijn Events" als je een bestaand event wil aanpassen. Ook kan je kijken voor welke feesten je 
            uitgenodigd bent, en welke open feesten er op het moment zijn!</p>
        </li>
        
        <li class="menublokken menublokken_foto">
        	<a class="menudetails" href="https://www.projects.science.uu.nl/INFOB1PICA/2015/02/www/images/Stap_2.PNG">
            	<img class="image_details" src="https://www.projects.science.uu.nl/INFOB1PICA/2015/02/www/images/Stap_2.PNG" />
            </a>
        </li>
           
	</ul>
    
    <ul class="menublok menublok_info">
    
    	<li class="menublokken menublokken_fotos">
        	<a class="menudetails" href="https://www.projects.science.uu.nl/INFOB1PICA/2015/02/www/images/Stap_3A.PNG">
        		<img class="image_details" src="https://www.projects.science.uu.nl/INFOB1PICA/2015/02/www/images/Stap_3A.PNG" />
            </a>
        	<a class="menudetails" href="https://www.projects.science.uu.nl/INFOB1PICA/2015/02/www/images/Stap_3B.PNG">
        		<img class="image_details" src="https://www.projects.science.uu.nl/INFOB1PICA/2015/02/www/images/Stap_3B.PNG" />
            </a>
        </li> 
          
        <li class="menublokken menublokken_info">
        	<h1 class="stap_1_h">Stap 3</h1>
        	<p>Geef je event een naam. Kies wat leuks, maar unieks en natuurlijk ook weer niet te lang. Niet ieder feest kan Project W heten!</p><BR>
        	<h1 class="stap_1_h">Of</h1>
      		<p>Kies een evenement dat je wilt bekijken. Mooie lijst met... Feestjes?</p>
        </li>
        
	</ul>
    
    <ul class="menublok menublok_info">
    
    	<li class="menublokken menublokken_info">
        	<h1 class="stap_1_h">Stap 4</h1>
        	<p>Kies welke modules je wilt gebruiken. De modules die je kiest, moeten eerst ingevuld worden, voordat ze werken. De volgende stappen kunnen overgeslagen worden, als je nog 
            geen modules wilt activeren.</p>
        	<h1 class="stap_1_h">Of</h1>
      		<p>Bekijk alle informatie van het feest waar je uitgenodigd bent. Als je zelf het feest hebt gemaakt, kan je ook nog de details wijzigen.</p>
        </li>
        
        <li class="menublokken menublokken_fotos">
        	<a class="menudetails" href="https://www.projects.science.uu.nl/INFOB1PICA/2015/02/www/images/Stap_4A.PNG">
        		<img class="image_details" src="https://www.projects.science.uu.nl/INFOB1PICA/2015/02/www/images/Stap_4A.PNG" />
            </a>
	 		<a class="menudetails" href="https://www.projects.science.uu.nl/INFOB1PICA/2015/02/www/images/Stap_4B.PNG">
        		<img class="image_details" src="https://www.projects.science.uu.nl/INFOB1PICA/2015/02/www/images/Stap_4B.PNG" />
        	</a>
        </li>
           
	</ul>
    
    <ul class="menublok menublok_info">
    
    	<li class="menublokken menublokken_foto">
        	<a class="menudetails" href="https://www.projects.science.uu.nl/INFOB1PICA/2015/02/www/images/Stap_5.PNG">
        		<img class="image_details" src="https://www.projects.science.uu.nl/INFOB1PICA/2015/02/www/images/Stap_5.PNG" />
            </a>
        </li>   
        
        <li class="menublokken menublokken_info">
        	<h1 class="stap_1_h">Stap 5</h1>
        	<p>Kies een datum. Hou rekening met planning en het afspreken met mensen. Niet iedereen kan snel tijd vrij maken.</p>
        </li>
        
	</ul>
    
    <ul class="menublok menublok_info">
    
    	<li class="menublokken menublokken_info">
        	<h1 class="stap_1_h">Stap 6</h1>
        	<p>Kies een locatie. Je kent het gezegde, "location, location, location"!</p>
        </li>
        
        <li class="menublokken menublokken_foto">
        	<a class="menudetails" href="https://www.projects.science.uu.nl/INFOB1PICA/2015/02/www/images/Stap_6.PNG">
        		<img class="image_details" src="https://www.projects.science.uu.nl/INFOB1PICA/2015/02/www/images/Stap_6.PNG" />
            </a>
        </li>
           
	</ul>
    
    <ul class="menublok menublok_info">
    
    	<li class="menublokken menublokken_foto">
        	<a class="menudetails" href="https://www.projects.science.uu.nl/INFOB1PICA/2015/02/www/images/Stap_7.PNG">
        		<img class="image_details" src="https://www.projects.science.uu.nl/INFOB1PICA/2015/02/www/images/Stap_7.PNG" />
            </a>
        </li>   
        
        <li class="menublokken menublokken_info">
        	<h1 class="stap_1_h">Stap 7</h1>
        	<p>Vul de namen en e-mail van je gasten in. Controleer de e-mailadressen en namen goed, altijd vervelend als je aan je moeder moet uitleggen wie dat 
        	meisje/die jongen nou weer is...</p>
        </li>
	
    </ul>
    
    <ul class="menublok menublok_info">
    	
        <li class="menublokken menublokken_info">
        	<h1 class="stap_1_h">Stap 8</h1>
        	<p>Hier kan je de kostenlijst invullen, met velden voor locatie, consumpties, etc. Misschien wel de belangrijkste van allemaal, dus check hem dubbel!</p>
        </li>
        
        <li class="menublokken menublokken_foto">
        	<a class="menudetails" href="https://www.projects.science.uu.nl/INFOB1PICA/2015/02/www/images/Stap_8.PNG">
        		<img class="image_details" src="https://www.projects.science.uu.nl/INFOB1PICA/2015/02/www/images/Stap_8.PNG"/>
            </a>
        </li>   
	
    </ul>
    
    <ul class="menublok menublok_info">
    	<li class="menublokken menublokken_foto">
        	<a class="menudetails" href="https://www.projects.science.uu.nl/INFOB1PICA/2015/02/www/images/Stap_9.PNG">
        		<img class="image_details" src="https://www.projects.science.uu.nl/INFOB1PICA/2015/02/www/images/Stap_9.PNG" />
            </a>
        </li>   
        
        <li class="menublokken menublokken_info">
        	<h1 class="stap_1_h">Stap 9</h1>
        	<p>Vul nog een mooie beschrijving in van het feest en bepaal of de informatie van het feest open is voor iedereen, of alleen voor genodigden.</p>
        </li>
	
    </ul>
    
    <ul class="menublok menublok_info">
    	
        <li class="menublokken menublokken_info">
        	<h1 class="stap_1_h">Stap 10</h1>
        	<p>Nog een laatste check, waarbij je kan kiezen wie je wel en wie je geen uitnodiging stuurt via e-mail. Twijfel je of je iets goed hebt ingevuld? Ga gerust terug en pas het aan. 
        	Zelfs als je een fout maakt, kan deze later nog aangepast worden.</p>
        </li>
        
        <li class="menublokken menublokken_foto">
        	<a class="menudetails" href="https://www.projects.science.uu.nl/INFOB1PICA/2015/02/www/images/Stap_10.PNG">
        		<img class="image_details" src="https://www.projects.science.uu.nl/INFOB1PICA/2015/02/www/images/Stap_10.PNG"/>
            </a>
        </li>   
	
    </ul>
    
    <p>En voilá! Je hebt je eigen event gemaakt! Nu kan je heerlijk achterover leunen en zien hoe het hele evenement zich voor je vormgeeft. Of wacht...</p>
    
    <a class="personen_info" name="anchor"></a>
    
    <h1>Natuurlijk is deze site niet uit het niets ontstaan</h1>
    <p>Deze site is met veel passie en plezier gemaakt door:</p>
    
    <ul class="menublok menublok_nieuw_op_site">
    	
        <li class="menublokken menublokken_foto">
        	<img class="image_details" src="https://scontent-ams3-1.xx.fbcdn.net/hphotos-xft1/v/t1.0-9/10646703_750888851645082_1097698482746988417_n.jpg?oh=8aa1cc4d6e4441edc896982e69d97fab&oe=5738402C"/>
        </li>   
        
        <li class="menublokken menublokken_info">
        	<h1 class="stap_3_h">Menno Jansen</h1>
        	<p class= "InfoText"> Samen met Floris heeft hij gewerkt aan de opmaak. Hij ook is onze javascripter. Verder heeft hij voor ons gecommunniceerd met verschillende 
            projectbeheerders. Menno is van alle markten thuis en heeft zich op alle fronten zichtbaar gemaakt.</p>
        </li>
	
    </ul>
    
    <ul class="menublok menublok_nieuw_op_site">
    	
        <li class="menublokken menublokken_info">
        	<h1 class="stap_3_h">Curtis Chin Jen Sem</h1>
        	<p class = "InfoText"> Curtis is de implementer die effectief het MVC model heeft toegepast binnen onze website. Dit alles in het Codeigniter framework. De backend heeft hij 
            samen met Nick uit de grond gestampt. Ook heeft hij zeer veel bugs gesquasht.
            </p>
        </li>
        
        <li class="menublokken menublokken_foto">
        	<img class="image_details" src="https://scontent-ams3-1.xx.fbcdn.net/hphotos-xap1/v/t1.0-9/19217_1619024801661602_6159233770696456683_n.jpg?oh=ea5d032314a29718bccdb6597112629d&oe=5744441A"/>
        </li>   
	
    </ul>
    
    <ul class="menublok menublok_nieuw_op_site">
    	
        <li class="menublokken menublokken_foto">
        	<img class="image_details" src="https://scontent-ams3-1.xx.fbcdn.net/hphotos-xpt1/v/t1.0-9/10730969_761630783908933_9074913153404355518_n.jpg?oh=e0aad87fd05eaeff6abe05340fb589fc&oe=5744A40D"/>
        </li>   
        
        <li class="menublokken menublokken_info">
        	<h1 class="stap_3_h">Tom Tanis</h1>
        	<p class= "InfoText">Tom heeft zich ingezet om de homepage zo overzichtelijk mogelijk te maken en van handige eigenschappen te voorzien. Ook heeft hij deze pagina gemaakt waar u
            nu op rondkijkt. Daarnaast heeft hij veel van de CSS en PHP geoptimaliseerd. </p>
        </li>
	
    </ul>
    
    <ul class="menublok menublok_nieuw_op_site">
    	
        <li class="menublokken menublokken_info">
        	<h1 class="stap_3_h">Wiebe de Vries</h1>
        	<p class= "InfoText">Wiebe heeft zich voornamelijk bezig gehouden met het documenteren en het presenteren van ons project. Ook heeft hij het begin aan de app gemaakt. Wegens
            tijdsgebrek is deze echter niet gerealiseerd.
            </p>
        </li>
        
        <li class="menublokken menublokken_foto">
        	<img class="image_details" src="https://scontent-ams3-1.xx.fbcdn.net/hphotos-frc3/v/t1.0-9/10256806_749421991756469_6591681934534047204_n.jpg?oh=3534f0d3f7150dec4f92b2b3f849be75&oe=56FE1986"/>
        </li>   
	
    </ul>
    
    <ul class="menublok menublok_nieuw_op_site">
    	
        <li class="menublokken menublokken_foto">
        	<img class="image_details" src="https://scontent-ams3-1.xx.fbcdn.net/hphotos-xft1/v/t1.0-9/11873447_645624265580719_8221927117802204665_n.jpg?oh=2712d428cf70396d5e5c177d48e0a407&oe=56FD9C7F"/>
        </li>   
        
        <li class="menublokken menublokken_info">
        	<h1 class="stap_3_h">Nick Elbertse</h1>
        	<p class= "InfoText">Nick heeft gewerkt aan de database implementatie en aan de toepassingen van het MVC model. De backend is dus flink door Nick onder handen genomen, met als
            resultaat een zeer mooie database die erg overzichtelijk is.
            </p>
        </li>
	
    </ul>
    
    <ul class="menublok menublok_nieuw_op_site">
    	
        <li class="menublokken menublokken_info">
        	<h1 class="stap_3_h">Floris Heijmans</h1>
        	<p class= "InfoText">Samen met Menno heeft deze jongeman menig uur gestoken in het mooi maken van deze website. Met zijn CSS en PHP skills heeft hij het begin van onze website
            neergezet en de standaarden hoog weten te houden.
            </p>
        </li>
        
        <li class="menublokken menublokken_foto">
        	<img class="image_details" src="https://scontent-ams3-1.xx.fbcdn.net/hphotos-xpt1/t31.0-8/s960x960/12493984_924799514276656_5423880653488313730_o.jpg"/>
        </li>   
	
    </ul>
</div>