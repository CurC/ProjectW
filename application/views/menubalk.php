<body>
    <div id="content">
		<div id="menubalk">
			<ul class="menu_lijst">
    			<li class="menu_lijst_blok" id="menu_lijst_blok_links">
                	<a href="<?php echo base_url('index.php/Main/events');?>">
                		<img class="menu_picture" src="<?php echo base_url('images/event.png');?>"/>
                    </a>
                </li>
    			
                <li class="menu_lijst_blok">
                	<a href="<?php echo base_url('index.php/Events/nieuw');?>">
                		<img class="menu_picture" src="<?php echo base_url('images/nieuw.png');?>" />
                    </a>
                </li>
    			
                <li class="menu_lijst_blok">
                	<a href="<?php echo base_url('index.php/Main/index'); ?>">
                    	<img class="menu_picture" src="<?php echo base_url('images/logo_big.png');?>"/>
                    </a>
                </li>
    			
                <li class="menu_lijst_blok">
                	<a href="<?php echo base_url('index.php/Main/info'); ?>">
                    	<img class="menu_picture" src="<?php echo base_url('images/info.png');?>"/>
                    </a>
                </li>
                
    			<li class="menu_lijst_blok" id="menu_lijst_blok_rechts">
                	<!--<div id="dropdown_wrap" onclick="displayDropdown()">-->
                    	<a class="inhoud_dropdown_block" onmouseover="displayDropdown()" onmouseleave='hideDropdown()' href="
						<?php 
        					if ($_SESSION["ingelogd"] == true){
            						echo base_url('index.php/Login/account');
        						} 
								else{
            						echo base_url('index.php/Login');
        						}
						?>">
        					<img class="dropdown_picture" src="
							<?php 
        						if ($_SESSION["ingelogd"] == true){
            						echo base_url('images/account.png');
        						} 
								else {
            					echo base_url('images/inloggen.png');
        						}
							?>"/>
                        </a>
                         
					<?php
						if ($_SESSION['ingelogd'] == true){
							echo "	<div class='dropdown_menu'>
										<ul class='dropdown'  onmouseover='displayDropdown()' onmouseleave='hideDropdown()'>
    									
											<li class='dropdown_block'>
												<a class='padding_knop' href='"; 
												echo base_url('index.php/Login/logUit');
												echo "' class='inhoud_dropdown_block'>
        											<img class='dropdown_picture' src='"; 
													echo base_url('images/uitloggen.png'); 
													echo "'/>
												</a>
											</li>
    									
											<li class='dropdown_block' id='layout_onderkant_dropdown'>
												<a class='padding_knop' href='"; 
												echo base_url('index.php/Login/wwchange'); 
												echo "' class='inhoud_dropdown_block'>
        											<img class='dropdown_picture' src='";
													echo base_url('images/ww_wijzigen.png'); 
													echo "'/>
												</a>
											</li>
									
										</ul>
									</div>";
						}
					?>
					<!--</div>-->
   				</li>
			</ul>
		</div>
	

<script>
	//Doet precies wat de naam suggereert, zorgt ervoor dat het dropdown menu wordt gedisplayed als je bent ingelogd en je hovert over uitloggen.
	//De tweede functie doet precies het omgekeerde.
	function displayDropdown(){
 var a = document.querySelector('.dropdown_menu');
 a.style.display = "block";
}
function hideDropdown(){
    var a = document.querySelector('.dropdown_menu');
    a.style.display = "none";
}
	
</script>