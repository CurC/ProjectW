<div id="hoofdtekst">
	<p>
		<h1>Gastenlijst</h1>
    	<?php 
    		echo $gast;
    	?>
        
        <form class="gasten_form" method="post" action="https://www.projects.science.uu.nl/INFOB1PICA/2015/02/www/index.php/Guestlist/toevoegen">
            <div class="layout_gasten">
            
                <div class="textfield_gasten">
                    <p>Naam:
                    	<input maxlength="80" type="text" name="naam" value="<?php echo $_SESSION["gnaam"];?>" placeholder="Naam">
                        	<div class="error_message">
								<?php echo $_SESSION["gnaamErr"];?>
                            </div>
                	</p>
                </div>
                
                <div>
                	<p>Mail adres:
                		<input maxlength="80" type="email" name="email" value="<?php echo $_SESSION["gemail"];?>" placeholder="Email">
                        	<div class="error_message">
								<?php echo $_SESSION["gemailErr"];?>
                            </div>
                	</p>
            	</div>
                
            </div>
            
            <input type="submit" class="verzendknop" value="Toevoegen">      
        </form>
        
        <form class="knoppen_gasten" action="https://www.projects.science.uu.nl/INFOB1PICA/2015/02/www/index.php/Guestlist/voltooid"> 
            <input type="submit" class="verzendknop" value="Voltooien"/>   
        </form>
        
        <form class="knoppen_gasten" action="https://www.projects.science.uu.nl/INFOB1PICA/2015/02/www/index.php/Events/mainEvent">
            <input type="submit" class="verzendknop" value="Terug">
        </form>
	</p>
</div>

<script type="text/javascript">
    function verwijderGast(gast_nummer)
    {
        var b = gast_nummer;
        
        var a = document.querySelectorAll('.gast_remove');
        
        console.log(a[0]);
        
        if(confirm('Weet je zeker dat u deze gast wilt verwijderen?')   == true){
        a[b].submit();
        }
    }
</script>