<div id="hoofdtekst">
	<p>
		<h1><?php echo $_SESSION['CurrentEventName'];?></h1>
		<p>Kies hier de modules die je wilt gebruiken in je evenement!</p>
		<div class="table">    
			<div class="tablerow">
    			<div class="left-cell">
                
        			<form method="post" action="<?php echo base_url('index.php/Events/buttonSwitch/DatumBool'); ?>">
        				<input type="submit" class="uit-knop" value="<?php echo $DatumBoolText;?>">
        			</form>
                    
    			</div>
                
    			<div class="middle-cell"> 
        		<?php
        			if ($_SESSION["DatumBool"] == 0){
            			echo "<p class='p_mainevent'>Datumprikker</p>";
        			} 
					else if($_SESSION["DatumBool"] == 1){
            		echo "<a class='link_mainevent' href=https://www.projects.science.uu.nl/INFOB1PICA/2015/02/www/index.php/Kalender>Datumprikker</a>";
        			}
        		?>
    			</div>
                
    			<div class="right-cell">
    			<?php 
       				echo $DatumVoltooid;
    			?>
    			</div>
			</div>
            
			<div class="tablerow">
    			<div class="left-cell">
                
        			<form method="post" action="<?php echo base_url('index.php/Events/buttonSwitch/LocatieBool'); ?>">
        				<input type="submit" class="uit-knop" value="<?php echo $LocatieBoolText;?>">
        			</form>
    			</div>
                
    		<div class="middle-cell"> 
        		<?php
        			if ($_SESSION["LocatieBool"] == 0){
            			echo "<p class='p_mainevent'>Locatiekiezer</p>";
        			} 
					else if($_SESSION["LocatieBool"] == 1){
            			echo "<a class='link_mainevent' href=https://www.projects.science.uu.nl/INFOB1PICA/2015/02/www/index.php/Location>Locatiekiezer</a>";
        			}
       			?>
    		</div>
    		
            <div class="right-cell">
    			<?php 
        			echo $LocatieVoltooid;
   				?>
    		</div>
		</div>
        
		<div class="tablerow">
    		<div class="left-cell">
        		
                <form method="post" action="<?php echo base_url('index.php/Events/buttonSwitch/GastenBool'); ?>">
        			<input type="submit" class="uit-knop" value="<?php echo $GastenBoolText;?>">
        		</form>
    		
            </div>
    		
            <div class="middle-cell"> 
        		<?php
        			if ($_SESSION["GastenBool"] == 0){
            			echo "<p class='p_mainevent'>Gastenlijst</p>";
        			} 
					else if($_SESSION["GastenBool"] == 1){
            			echo "<a class='link_mainevent' href=https://www.projects.science.uu.nl/INFOB1PICA/2015/02/www/index.php/Guestlist>Gastenlijst</a>";
        			}
        		?>
    		</div>
            
    		<div class="right-cell">
    			<?php 
        			echo $GastenVoltooid;
    			?>
    		</div>
		</div>
        
		<div class="tablerow">
    		<div class="left-cell">
        		<form method="post" action="<?php echo base_url('index.php/Events/buttonSwitch/KostenBool'); ?>">
       				<input type="submit" class="uit-knop" value="<?php echo $KostenBoolText;?>">
        		</form>
    		</div>
            
    		<div class="middle-cell"> 
        		<?php
        			if ($_SESSION["KostenBool"] == 0){
            			echo "<p class='p_mainevent'>Kostenlijst</p>";
        			} 
					else if($_SESSION["KostenBool"] == 1){
            			echo "<a class='link_mainevent' href=https://www.projects.science.uu.nl/INFOB1PICA/2015/02/www/index.php/Costlist>Kostenlijst</a>";
        			}
        		?>
    		</div>
            
    		<div class="right-cell">
    			<?php 
        			echo $KostenVoltooid;
   				?>
    		</div>
		</div>

		<p>Vul hieronder de beschrijving van je evenement in!</p>

    	<form class="verzend_mainevent" id="verzend_je_event" method="post" action="<?php echo base_url('index.php/Events/verzendcheck');?>">
    		<textarea maxlength="240" class="textfield_beschrijving" rows="15" cols="50" name="eventBeschrijving"><?php 
					if (isset($_SESSION["beschrijving_tekst"])){echo $_SESSION["beschrijving_tekst"];
					}
				?></textarea>
            
    		<div class="mainevent_checkbox">   
                <label for="OEvent">Klik het vakje hiernaast als u uw event open wilt maken <br> (LET OP: uw event zal dan voor iedereen te zien zijn!)</label>
                <input type="checkbox" name="OEvent" id="OEvent" value="1" <?php echo $checked ?>>
                <p class="error_message" id="error_tekst_voltooiknop"><?php echo $_SESSION['OpenEvenementErr']; ?></p>
    		</div>
            
        	
        	<input type="button" onclick="submitForm()" class="verzendknop" id="verzend_event" value="Voltooien">
    	</form>
        
    	<form class="verzend_mainevent" action="https://www.projects.science.uu.nl/INFOB1PICA/2015/02/www/index.php/Events/verwijder/<?php echo $_SESSION['CurrentEventID']; ?>">
        	<input type="submit" class="verzendknop" value="Verwijder event">
    	</form>
        
    	<form class="verzend_mainevent" action="https://www.projects.science.uu.nl/INFOB1PICA/2015/02/www/index.php/Events/overzicht">
        	<input type="submit" class="verzendknop" value="Terug naar overzicht">
    	</form>
	</div>
</div>

<script type="text/javascript">
    
    function submitForm(){ 
		var a = <?php echo $DatumVoltooidwaarde; ?>;
		var b = <?php echo $LocatieVoltooidwaarde; ?>;
		var c = <?php echo $GastenVoltooidwaarde; ?>;
		var d = <?php echo $KostenVoltooidwaarde; ?>;

		if (a+b+c+d == 4){
			document.getElementById("verzend_je_event").submit();
		}
        else{
			document.getElementById("error_tekst_voltooiknop").textContent = "Voltooi eerst de gekozen modules!";
        }
    }
    
    function changeButtonClass(){
        var a = [].slice.apply(document.getElementsByClassName("uit-knop"));
		
        if (a != null){
            for (var i = 0; i<a.length; i++){
                if (a[i].value == "Aan"){
                    a[i].className = "aan-knop";               
				}
                else if(a[i].value == "Uit"){
                    a[i].className = "uit-knop";
                }
            }
        } 
    }
	window.onload = changeButtonClass;
	
</script>