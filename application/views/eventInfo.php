<script type="text/javascript" >

    function maakFinal(dNummer){
  
        var b = dNummer;
        var a = document.getElementsByClassName('finalform');
        
        if(confirm('Weet je zeker dat je deze datum wilt kiezen? Er wordt ook een mail naar de gasten gestuurd.')   == true){
            a[b].submit(); 
        }
    }
	
</script>

<div id="hoofdtekst">
    <h1><?php echo $EventNaam;?></h1>
    
    <ul class="menublok menublok_landing">
    	<li class= "menublokken menublokken_header" id= "beschrijving_header">
        	<h1 class="stap_2_h">Beschrijving:</h1>
        </li>
           
        <li class= "menublokken menublokken_tekst">
        	<p><?php 
				if($beschrijving == null){
					echo '<div class="error_message">Geef een beschrijving aan je event!</div>';
				}
   				else {
					echo wordwrap($beschrijving, 120 ,"<br>\n", true);
				}
    		?>
       	</li>
	</ul>
    
    <ul class="menublok menublok_landing">
    
    	<li class= "menublokken menublokken_header">
        	<h1 class="stap_1_h">Locatie:</h1>
        </li>   
        
        <li class= "menublokken menublokken_tekst">
        	<p><?php 
				if($locatie == null){
					echo '<div class="error_message">Er is geen locatie gekozen!</div>';
				}
    			else {
					echo $locatie;
				} 
			?></p>
        </li>
        
	</ul>
    
    <ul class="menublok menublok_landing">
    
    	<li class= "menublokken menublokken_header">
        	<h1 class="stap_1_h">Uitgenodigde gasten:</h1>
        </li>
           
        <li class= "menublokken menublokken_gasten">
        	<p>
        		<div class='tabelrij_gasten1'>
                    <div class='linkercell_gasten'>
                    	<strong>Naam</strong>
                    </div>
                    
                    <div class='rechtercell_gasten'>
                    	<strong>Aanwezigheid</strong>
                    </div>
                    
    				<div class='rechtercell_eventinfo'> 
						<?php echo $gdata1;?> 
            		</div>
          	  
    				<div class='rechtercell_eventinfo'>  
						<?php echo $gdata2;?> 
            		</div>
            	
    				<div class='rechtercell_eventinfo'>  
						<?php echo $gdata3;?> 
        	    	</div>
        	    
    				<div class='rechtercell_eventinfo'>  
						<?php echo $gdata4;?> 
        	    	</div>
        	    
        		</div>
		 		<?php 	
					if($gast == null){ 
						echo '<p class="error_message">Er zijn nog geen gasten uitgenodigd</p>';
					}
  					else {
						echo $gast;
					} 
				?>
                
         		<div class='tabelrij_gasten1'>
        
                    <div class='linkercell_gasten'>
                    	<strong>Totaal Aanwezig</strong>
                    </div>
                    
                   	<div class='rechtercell_gasten'>
                   		<strong></strong>
                    </div>
                    
    				<div class='rechtercell_eventinfo'> 
						<?php echo $tdata1;?> 
            		</div>
            
    				<div class='rechtercell_eventinfo'>  
						<?php echo $tdata2;?> 
            		</div>
            
    				<div class='rechtercell_eventinfo'>  
						<?php echo $tdata3;?> 
            		</div>
            
    				<div class='rechtercell_eventinfo'>  
						<?php echo $tdata4;?> 
            		</div>
            
         		</div>

            	<div class='tabelrij_gasten1'>
        
                    <div class='linkercell_gasten'>
                    	<strong>Kies hier je datum definitief</strong>
                    </div>
                    
                    <div class='rechtercell_gasten'>
                    	<strong></strong>
                    </div>
                    
    			    <?php 
                    echo $final1; 
                    echo $final2;
                    echo $final3;
                    echo $final4;?>
            
         		</div>
         	</p>
        </li>
         
	</ul>
    
    <ul class="menublok menublok_landing">
    
    	<li class= "menublokken menublokken_header">
        	<h1 class="stap_1_h">Kosten:</h1>
        </li>
           
        <li class= "menublokken menublokken_gasten">
        	<p>
        		<div class='tabelrij_gasten1'>
					<div class='linkercell_kosten'>
    					<strong>Kostbeschrijving</strong>
					</div>

					<div class='rechtercell_kosten'>
    					<strong>Kostprijs</strong>
					</div>
				</div>
            	<?php 
    				if($costs == null){
						echo '<p class="error_message">Er zijn nog geen kosten ingevoerd</p>';
					}
    				else 
    				{
        				echo $costs;
        				echo $totaal;
    				}
    			?>
            </p>
        </li>
        
	</ul>
    
	<form class="verzend_mainevent" action="https://www.projects.science.uu.nl/INFOB1PICA/2015/02/www/index.php/Events/overzicht">
        <input type="submit" class="verzendknop" value="Terug naar overzicht">
    </form>
    
	<form class="verzend_mainevent" action="<?php echo $url ?>">
        <input type="submit" class="verzendknop" value="Wijzig event">
    </form>
    
</div>