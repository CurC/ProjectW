<div id="hoofdtekst">
	<p>Weet je zeker dat je de volgende mensen uit wilt nodigen voor je event:

    	<div class='tabelrij'>
        	<div class='linkercell'>
            	<strong>Naam</strong>
        	</div>
            
        	<div class ='middencell'>
            	<strong>Mail</strong>
        	</div>
        	<div class ='rechtercell'>
            	<strong>Wel / Niet uitnodigen</strong>
        	</div>
    	</div>
        
    	<?php 
    		echo $gast;
    	?>
    
	</p>
    
	<p style="margin-top: 20px;">En dat de volgende beschrijving klopt?</p>
    
	<?php 
    	echo wordwrap($beschrijving, 120 ,"<br>\n", true);
    ?>
    
    <form class="knoppen_gasten" action="https://www.projects.science.uu.nl/INFOB1PICA/2015/02/www/index.php/Events/mainEvent">
    	<input type="submit" class="verzendknop" value="Nee, ga terug">
    </form>
    
    <form class="verzend_mainevent" action="<?php echo base_url('index.php/Events/verzendmails/'); ?>">
        <input type="submit" class="verzendknop" onclick="alert('Mails zijn verzonden!')" value="Ja, verstuur e-mails!">
    </form>    
</div>