<div id="hoofdtekst">
	<h1>Je bent uitgenodigd voor:</h1>
	<p class= "landing_event"><?php echo $_SESSION['eventname'];?></p>
    
	<h1>Door:</h1>
	<p class= "landing_organiser"><?php echo $_SESSION['hostvoor']." ".$_SESSION['hostachter'];?></p>
    
	<h1>Je kunt de organisator bereiken op:</h1>
	<p class= "landing_email"><?php echo $_SESSION['hostmail'];?></p>
  
    <ul class="menublok menublok_landing">
    
    	<li class= "menublokken menublokken_header" id= "beschrijving_header">
        	<h1 class="stap_2_h">Beschrijving:</h1>
        </li>
           
        <li class= "menublokken menublokken_tekst">
        	<p><?php 
				if($beschrijving == null){
					echo "Er is geen beschrijving ingevuld!";
				}
				else {
					echo $beschrijving;
				};?>
            </p>
        </li>
	</ul>   
     
    <ul class="menublok menublok_landing">
    
    	<li class= "menublokken menublokken_header">
        	<h1 class="stap_1_h">Locatie:</h1>
        </li>
           
        <li class= "menublokken menublokken_tekst">
        	<p><?php 
				if($location == null){
					echo "Er is geen locatie gekozen!";
				}
				else {
					echo $location;
				}?>
            </p>
        </li>
        
	</ul>
    

	<ul class="menublok menublok_landing">
    
    	<li class= "menublokken menublokken_header">
        	<h1>Ben je aanwezig?</h1>
        </li>   
        
        <li class= "menublokken menublokken_form">
        	<div class="aanwezig"> 
        		<form class="aanwezigheidsform" method="post" action="<?php echo base_url('index.php/Guestlist/aanwezig'); ?>">
            		<input type="radio" name="aanwezig" value="true" placeholder="Ja" <?php echo $checkedAanwezigJa; ?>>Ja</input>
            		<input type="radio" name="aanwezig" value="false" placeholder="Nee" <?php echo $checkedAanwezigNee; ?>>Nee</input>
                
        	</div>
        </li>
        
    </ul>
    
    <ul class="menublok menublok_landing">
    	
        <li class= "menublokken menublokken_header">
        	<h1>Welke dagen kun je?</h1>
        </li>
           
        <li class= "menublokken menublokken_datum">
        	<div class="datum"> 
      			<div><?php 
					if($gdata1 != 0){
    					echo  "<input type='checkbox' name='datum1' value='A' placeholder=''" . $checkedDatum1 . ">";
					}?>
				<?php 
					if($gdata1 != 0){
    					echo $gdata1;
					}?>
        	    </div>

       		 	<div><?php 
					if($gdata2 != 0){
    					echo  "<input type='checkbox' name='datum2' value='B' placeholder=''" . $checkedDatum2 . ">";
					}?>
				<?php 
					if($gdata2 != 0){
						echo $gdata2;
					}?>
           		</div>
   
        	</div>
        </li>
        
       	<li class="menublokken menublokken_datum">
        	<div class="datum">
            
        		<div><?php 
					if($gdata3 != 0){
    					echo  "<input type='checkbox' name='datum3' value='C' placeholder=''" . $checkedDatum3 . ">";
					}?>
				<?php 
					if($gdata3 != 0){
						echo $gdata3;
					}?>
                </div>
                
				<div><?php 
					if($gdata4 != 0){
    					echo  "<input type='checkbox' name='datum4' value='D' placeholder=''" . $checkedDatum4 . ">";
					}?>
				<?php 
					if($gdata4 != 0){
						echo $gdata4;
					}?>
                </div>
   			</div>
        </li>
	</ul>    
    
    <p class="submit">
    	<input type="submit" class="verzendknop" onclick="alert('Je aanwezigheid is opgeslagen!')" name="commit" value="Verzenden">
</p></form>
     
</div>