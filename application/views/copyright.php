<div id="hoofdtekst">
	<h1>Copyright</h1>
	<p>Bron en license van afbeelding van Open event knop op /Main/events:
    
    	<ul>
        
        	<li>
    			<a href="https://www.flickr.com/photos/52170719@N08/7129476615">Source</a>
            </li>
            
        	<li>
    			<a href="https://creativecommons.org/licenses/by-sa/2.0/">License</a>
            </li>
            
		</ul>
	Alle anderen afbeeldingen hebben een gratis license.</p>
</div>