<div id="hoofdtekst" >
    <h1>Log in</h1>
	<form class="login_form" method="post" action="<?php echo base_url('index.php/Login/checklogin'); ?>">
        	<div class="login_textfields" >
        <input id="login_bovenste_textfield" size="32" maxlength="80" type="email" name="login" value="<?php echo $email;?>" placeholder="Email">
            <div class="error_message">
				<?php echo $emailErr;?>
            </div>
        
        	<input maxlength="80" type="password" name="password" placeholder="Password">
            <div class="error_message">
				<?php echo $passErr;?>
				<?php echo $activatieErr;?>
            </div>
        </div>
    	<p class="remember_me">
    		<label>
        		<input type="checkbox" name="remember_me" id="remember_me" value="on">
                Onthoud mij op deze computer<br> (U geeft toestemming voor cookies)
    		</label>
    	</p>
        <input type="submit" class="verzendknop" name="commit" value="Login">        
    </form>
    
    <form id="" action="<?php echo base_url('index.php/Login/veranderpass'); ?>">
    	<input type="submit" class="verzendknop" value="Wachtwoord vergeten?">
    </form>    
    
    	<form style="margin-bottom:20px;" action="<?php echo base_url('index.php/Login/registratie'); ?>">
    		<input type="submit" class="verzendknop" value="Registreren">
    	</form>
</div>