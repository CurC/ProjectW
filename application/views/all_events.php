<div id="hoofdtekst">

	<h1>Overzicht Open Evenementen</h1>
    
	<div id="my_events"> 
		<?php echo $OEvents; ?>
	</div>
     
	<p><?php if(isset($_SESSION['latit']))echo $_SESSION['latit']; ?></p>
    
	<form id="locatie-doorgeven" style="display: none" method = "post" action= "<?php echo base_url('index.php/Openevents/index');?>">
		<input type="text" name="lat" id="locatie-lat" value=""></input>
		<input type="text" name="lon" id="locatie-lon" value=""></input>
	</form>
    
	<button id="buurt" class="verzendknop" onclick="getLocation()">Zie alleen evenementen bij jou in de buurt!</button>
    
	<?php if(isset($infoBuurt))echo '<p>' . $infoBuurt . '</p>'; ?>
    
	<button id="alles" class="verzendknop" onclick="displayAll()">Zie alle evenementen!</button>
    
</div>

<script>

	var opties = {
		enableHighAccuracy: true
	};
	var knop1 = document.getElementById(buurt);
	var knop2 = document.getElementById(alles);
	var x = 0;

	function error(err) {
  		console.warn('ERROR(' + err.code + '): ' + err.message);
	}
	
	function getLocation(){
		if (navigator.geolocation){
			navigator.geolocation.getCurrentPosition(initialize, error, opties);
		}
	}

	function displayAll(){
    	window.location.href = "https://www.projects.science.uu.nl/INFOB1PICA/2015/02/www/index.php/Openevents/index";
	}    
    
	function initialize(position){
		var lat = position.coords.latitude;
		var lon = position.coords.longitude;
	
		document.getElementById('locatie-lat').value = lat;
		document.getElementById('locatie-lon').value = lon;
		document.getElementById('locatie-doorgeven').submit();
	}
	
	function showMoreOrLess(){
		if(x=1){
			buurt.style.display="none";
			alles.style.display="inline-block";
			x = x-1;
		}
		else{
			buurt.style.display="inline-block";
			alles.style.display="none";
			x = x+1;
		}
	}

</script>