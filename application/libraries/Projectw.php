<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Projectw {

    // Kijkt of de variabele bestaat, 
    // als niet niet dan word 't gecreeerd 
    public function _initVar(&$data, $var) 
    {    
        if (isset($_SESSION["$var"])) {
            $data["$var"] = $_SESSION["$var"];
        } else {
            $_SESSION["$var"] = "";
            $data["$var"] = "";
        }
    }
    
    public function _resetVar($var) 
    {    
        $_SESSION["$var"] = "";
    }

    public function _key()
    {
        return "paard";
    }
}