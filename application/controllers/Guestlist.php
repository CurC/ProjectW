<?php
class Guestlist extends MY_Controller {
    
    public function index()
    {
        $data['title'] = "Gastenlijst";
        $this->_initGuestVars($data);
        $data['gast'] = '';
        $i = 0;
        foreach ($this->database_model->getGuestEvent($_SESSION["CurrentEventID"]) as $row) 
        {
            $gastid = $row['GastID'];
            $gastAanwezig = $row['Aanwezig'];
            $gastNaam = $row['GastNaam'];
            
            if ($row['Aanwezig'] == 1)
            {
                $gastAanwezig = "Van de partij!";
            }
            else if ($row['Aanwezig'] == 0)
            {
                $gastAanwezig = "Nog niet gereageerd.";
            }
            else if ($row['Aanwezig'] == 2)
            {
                $gastAanwezig = "Sjaak Afhaak :(";
            }
                        
            foreach($this->database_model->getGuestByID($gastid) as $gastrow)
            {
                //$gastNaam = $gastrow['GastNaam'];
                $gastMail = $gastrow['GastMail'];

                $data['gast'] = $data['gast'] . "
                	<div class='inputbox tabelrij'>
					
                    	<div class ='linkerlinkercel'>
                        	<form class='gast_remove' action='https://www.projects.science.uu.nl/INFOB1PICA/2015/02/www/index.php/Guestlist/remove/" . $gastid . "'>
								<input type='button' class='verwijderknop' onClick='verwijderGast(" . $i . ")' value=''>
							</form>
                    	</div>
						
                    	<div class='guestcell_links'>
							" . $gastNaam . "
                 	   </div>
					   
               	     <div class ='guestcell_midden'>
							" . $gastMail . "
                	    </div>
						
                	    <div class ='guestcell_rechts'>
							" . $gastAanwezig . "
                	    </div>
						
                	</div>
                ";
                $i = $i + 1;
            }
        }

        $this->load->view('header.php', $data);
        $this->load->view('menubalk.php');
        $this->load->view('gastenlijst.php', $data);
        $this->load->view('footer.php');
    }

    public function remove($gastID){
        $this->database_model->removeGuestEvent($_SESSION['CurrentEventID'], $gastID);
        redirect('index.php/Guestlist');
    }

    public function toevoegen(){

        if (empty($this->input->post("naam"))) 
        {
            $_SESSION["gnaamErr"] = "Vul een naam in.";
            $_SESSION["gnaam"] = "";
            
            if (empty($this->input->post("email"))) 
            {
                $_SESSION["gemailErr"] = "Er is een emailadres nodig";
                redirect('index.php/Guestlist');
            } 
            else 
            {   
                $gemail = $this->input->post("email");
                $_SESSION["gemailErr"] = "";
                $_SESSION["gemail"] = $gemail;
            }
            redirect('index.php/Guestlist');
        } 
        else 
        {
            $gnaam = $this->input->post("naam");
            $_SESSION["gnaamErr"] = "";
            $_SESSION["gnaam"] = $gnaam;
        }

        if (empty($this->input->post("email"))) 
        {
            $_SESSION["gemailErr"] = "Er is een emailadres nodig";
            redirect('index.php/Guestlist');
        } 
        else 
        {   
            $gemail = $this->input->post("email");
            $_SESSION["gemailErr"] = "";
            $_SESSION["gemail"] = $gemail;
        }

        $gastnaam = $gnaam;
        $gastemail = $gemail;
        $emailbezet = FALSE;

        foreach($this->database_model->getGuestByEmail($gastemail) as $row)
        {
            if ($gastemail == $row['GastMail'])
            {
                $emailbezet = TRUE;
            }
        }

        if ($emailbezet)
        {
            $gastid = $row['GastID'];

            $this->database_model->insertGuestEvent($_SESSION["CurrentEventID"], $gastid, $gastnaam);
        } 
        else 
        {   
            $this->database_model->insertGuest($gastnaam, $gastemail);
            foreach ($this->database_model->getGuestByEmail($gemail) as $row)
            {
                $result = $row['GastMail'];
                if ($result === $gemail) 
                {
                    $gastid = $row['GastID'];
                }
            }
            $this->database_model->insertGuestEvent($_SESSION["CurrentEventID"], $gastid, $gastnaam);
        }

        $this->_resetGastVar();
        redirect('index.php/Guestlist');
    }

    public function voltooid()
    {
        $this->database_model->updateGuestVoltooid($_SESSION["CurrentEventID"]);

        foreach ($this->database_model->getGuestEvent($_SESSION["CurrentEventID"]) as $row) 
        {
            $gastid = $row['GastID'];
            $gastAanwezig = $row['Aanwezig'];

         /*   foreach($this->database_model->getGuestByID($gastid) as $gastrow)
            {
                $gastNaam = $gastrow['GastNaam'];
                $gastMail = $gastrow['GastMail'];
                $this->sendInvite("$gastMail");
            }*/
        }

        redirect('index.php/Events/mainEvent');
    }

    
    public function landing($eventid, $gastid, $key)
    {

        $levent = htmlspecialchars($eventid); 
        $lgast = htmlspecialchars($gastid); 
        $data['checkedAanwezigJa'] = "";
        $data['checkedAanwezigNee'] = "";
        
        for ($i = 1; $i <= 4; $i++) 
        {
            $data["checkedDatum" . $i] = "";
        }
        
        if ($key !== md5($gastid . $this->projectw->_key()))
        {
            redirect('index.php/Main/index');
        }

        //haal eventname en host uit db
        foreach($this->database_model->getEventNameHost($levent) as $row){
            $eventName = $row['EventName'];
            $eventHostID = $row['UserID'];
        }
        
        foreach($this->database_model->getNameHost($eventHostID) as $row){
            $hostvoor = $row['Voornaam'];
            $hostachter = $row['Achternaam'];
            $hostmail = $row['Email'];
        }
        
        foreach($this->database_model->getLocation($levent) as $row){
            $location = $row['Locatie'];
        }
        
        foreach($this->database_model->getBeschrijving($levent) as $row){
            $beschrijving = $row['Beschrijving'];
        }
        
        foreach ($this->database_model->getGuestEventByGastIDAndEventID($lgast, $levent) as $row) 
        {
            for ($i = 1; $i <= 4; $i++) 
            {
                if ($row["Datum" . $i] === "1")
                {
                    $data["checkedDatum" . $i] = "checked='checked'";
                }
            }

            if ($row["Aanwezig"] === "1")
            {
                $data["checkedAanwezigJa"] = "checked='checked'";
            }
            else
            {
                $data["checkedAanwezigNee"] = "checked='checked'"; 
            }
        }
        
        foreach ($this->database_model->getDataByEventID($levent) as $row) 
        {

            for($i = 1; $i <= 4; $i++)
            {
                ${"gdata" . $i} = $row['Data' . $i];
                if (${"gdata" . $i} != 0)
                {
                    ${"gdata" . $i . "Datum"} = date_create_from_format('U', ${"gdata" . $i} + 3600);
                    $data['gdata' . $i] = ${"gdata" . $i . "Datum"}->format('d F Y');
                }
                else if (${"gdata" . $i} == 0)
                {
                    $data['gdata' . $i] = 0;
                }
            }

            $data["gtest"] = "hallo";
        }
        
        $data['beschrijving'] = $beschrijving;
        $data['location'] = $location;
        $_SESSION['hostvoor'] = $hostvoor;
        $_SESSION['hostachter'] = $hostachter;
        $_SESSION['hostmail'] = $hostmail;
        $_SESSION['levent'] = $levent;
        $_SESSION['lgast'] = $lgast;
        $_SESSION['eventname'] = $eventName;
        $_SESSION['hostID'] = $eventHostID;
        $data['title'] = "Uitnodiging";
        $this->load->view('header.php',$data);
        $this->load->view('menubalk.php');
        $this->load->view('landingpage.php', $data);
        $this->load->view('footer.php');
    }

    
    public function aanwezig(){
        
        if (($this->input->post('aanwezig')) == "true")
        {
            //zet op aanwezig
            $this->database_model->setAanwezig($_SESSION['lgast'], $_SESSION['levent']);
            //redirect("index.php");
        }
        else
        {
            //zet op afwezig
             $this->database_model->setAfwezig($_SESSION['lgast'], $_SESSION['levent']);
            //redirect("index.php");
        }
        
        
        if (!empty($this->input->post('datum1')))
        {
            $this->database_model->setD1J($_SESSION['lgast'], $_SESSION['levent']);
        }
        else
        {
            $this->database_model->setD1N($_SESSION['lgast'], $_SESSION['levent']);
        }
        if (!empty($this->input->post('datum2')))
        {
            $this->database_model->setD2J($_SESSION['lgast'], $_SESSION['levent']);
        }
        else
        {
            $this->database_model->setD2N($_SESSION['lgast'], $_SESSION['levent']);
        }
        if (!empty($this->input->post('datum3')))
        {
            $this->database_model->setD3J($_SESSION['lgast'], $_SESSION['levent']);
        }
        else
        {
            $this->database_model->setD3N($_SESSION['lgast'], $_SESSION['levent']);
        }
        if (!empty($this->input->post('datum4')))
        {
            $this->database_model->setD4J($_SESSION['lgast'], $_SESSION['levent']);
        }
        else
        {
            $this->database_model->setD4N($_SESSION['lgast'], $_SESSION['levent']);
        }
        
        redirect("index.php");
    }
    

    public function _initGuestVars(&$data)
    {
        $this->projectw->_initVar($data, 'gnaam');
        $this->projectw->_initVar($data, 'gemail');
        $this->projectw->_initVar($data, 'gnaamErr');
        $this->projectw->_initVar($data, 'gemailErr');
    }

    private function _resetGastVar()
    {
        $this->projectw->_resetVar('gnaam');
        $this->projectw->_resetVar('gnaamErr');
        $this->projectw->_resetVar('gemail');
        $this->projectw->_resetVar('gemailErr');
    }
}
?>