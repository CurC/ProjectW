<?php
class Kalender extends MY_AUTH{
	
	//Deze functie wordt geladen als de pagina wordt geladen
	public function index($year = null, $month = null){
	  	$this->load->model('database_model');


		//Haalt bestaande data op uit de database zet ze om in een string die te printen is!
		foreach ($this->database_model->getDataByEventID($_SESSION["CurrentEventID"]) as $row){
			$data1 = $row['Data1'];
			$data2 = $row['Data2'];
			$data3 = $row['Data3'];
			$data4 = $row['Data4'];

			if ($data1 != 0){
			  $data1Datum = date_create_from_format('U', $data1 + 3600);
			  $data['data1'] = $data1Datum->format('d F Y');
			}

			if ($data2 != 0){
			  $data2Datum = date_create_from_format('U', $data2 + 3600);
			  $data['data2'] = $data2Datum->format('d F Y');
			}
			
			if ($data3 != 0){
			  $data3Datum = date_create_from_format('U', $data3 + 3600);
			  $data['data3'] = $data3Datum->format('d F Y');
			}
			
			if ($data4 != 0){
			  $data4Datum = date_create_from_format('U', $data4 + 3600);
			  $data['data4'] = $data4Datum->format('d F Y');
			}
			
			$data["test"] = "hallo";
			
		}
		
		$data["test"] = "hallo2";

		//Als het een openevenement is, dan komt er nog wat extra info bij
		$openevenement = FALSE;
		foreach ($this->database_model->getOpenEvents() as $row){
			
			if ($_SESSION["CurrentEventID"] == $row['EventID']){
				$openevenement = TRUE;
			}
		}
		
        $data['Open_info'] = '';
		
		if ($openevenement === TRUE){
			$data['Open_info'] = "<p>Bij een open evenement wordt alleen gekeken naar de voorkeursdatum.</p>";	
		}
		
		//Kalender wordt hieronder gegenereert met behulp van de CodeIgniter library
		//Niet teveel aan doen
		$this->lang->load('calendar', 'english');
		$this->load->library('parser');
		$pref = array ('show_next_prev'=>TRUE,
					   'next_prev_url'=> base_url().'index.php/Kalender/index',
					   'show_other_days' => TRUE);

		  //De template van de kalender
		$pref['template'] = '

		{table_open}<table border="0" cellpadding="5" cellspacing="0" id= "kalender">{/table_open}

		{heading_row_start}<tr id= "kalender-header">{/heading_row_start}

		{heading_previous_cell}<th><a class="vorige_volgende" href="{previous_url}">&lt;&lt;</a></th>{/heading_previous_cell}
		{heading_title_cell}<th colspan="{colspan}" id = "kalender-maand">{heading}</th>{/heading_title_cell}
		{heading_next_cell}<th><a class="vorige_volgende" href="{next_url}">&gt;&gt;</a></th>{/heading_next_cell}

		{heading_row_end}</tr>{/heading_row_end}

		{week_row_start}<tr id= "kalender-weekrow">{/week_row_start}
		{week_day_cell}<td class = "kalender-weekcell"><b>{week_day}</b></td>{/week_day_cell}
		{week_row_end}</tr>{/week_row_end}

		{cal_row_start}<tr class= "kalender-row">{/cal_row_start}
		{cal_cell_start}<td class="kalender-cell">{/cal_cell_start}
		{cal_cell_start_other}<td class="kalender-cell-ander">{/cal_cell_start_other}


		{cal_cell_content}{day}{/cal_cell_content}
		{cal_cell_content_today}<div class="highlight"><span href="{content}">{day}</span></div>{/cal_cell_content_today}

		{cal_cell_no_content}<span onclick="datum(this)" href="#" class = "kalender-cell-link">{day}</span>{/cal_cell_no_content}
		{cal_cell_no_content_today}<div class="highlight" onclick="datum(this)" href="#">{day}</div>{/cal_cell_no_content_today}

		{cal_cell_blank}{/cal_cell_blank}
		{cal_cell_other}{day}{/cal_cel_other}

		{cal_cell_end}</td>{/cal_cell_end}
		{cal_cell_end_other}</td>{/cal_cell_end_other}
		{cal_row_end}</tr>{/cal_row_end}

		{table_close}</table>{/table_close}
		';

		$this->load->library('calendar', $pref);
		$data['kalender'] = $this->calendar-> generate($year, $month);
		$data['title'] = "Kalender";
		$data['event'] = $_SESSION["CurrentEventID"];
		$this->load->view('header.php', $data);
		$this->load->view('menubalk.php');
		$this->load->view('kalender.php', $data);
		$this->load->view('footer.php');

		

	}

	//Zet de nieuw gekozen data in de database, werkt lastig dus laten zoals het is.
	//+36000 vanwege tijdzones enzo
	public function loadSession()
	{
	  	//date_default_timezone_set('Europe/Amsterdam');
	  	$this->load->model('database_model');
	  	$datada = $this->uri->segment(3);
		$datumdate = strtotime($datada) + 36000;
	  
	  	if ($this->database_model->checkData($_SESSION["CurrentEventID"]) == false)
		{
			$this->database_model->insertIntoData($_SESSION["CurrentEventID"], $datumdate, 1);
	  	}
	  	else
		{

			foreach ($this->database_model->getDataByEventID($_SESSION["CurrentEventID"]) as $row)
			{
				$data1 = $row['Data1'];
				$data2 = $row['Data2'];
				$data3 = $row['Data3'];
				$data4 = $row['Data4'];
			
				if ($datumdate == $data1 || $datumdate == $data2 || $datumdate == $data3 || $datumdate == $data4)
				{
					redirect('index.php/Kalender/errorMess');
				}

				if ($data1 == 0)
				{
					$this->database_model->insertIntoData($_SESSION["CurrentEventID"], $datumdate, 1);
					$data["test"] = "ja";
				}
				else if ($data2 == 0)
				{
					$this->database_model->insertIntoData($_SESSION["CurrentEventID"], $datumdate, 2);
				}
				else if ($data3 == 0)
				{
					$this->database_model->insertIntoData($_SESSION["CurrentEventID"], $datumdate, 3);
				}
				else if ($data4 == 0)
				{
					$this->database_model->insertIntoData($_SESSION["CurrentEventID"], $datumdate, 4);
				}
				else 
				{
						$data["test"] = "nee";
				}

	  		}
	  	}
		//Link door naar de goede maand / het goede jaar
		$datag = explode("-", $datada);
		$maandInt = date('m', strtotime($datag[1]));
		$url = 'index.php/Kalender/index/' . $datag[2] . '/' . $maandInt;
		redirect($url);

	}
	//Verwijdert alle data uit de database
	public function resetDates()
	{
		$this->load->model('database_model');
		$this->database_model->removeData($_SESSION["CurrentEventID"]);
		redirect('index.php/Kalender/index');
	}
	//Geladen als je een dubble datum kiest
	public function errorMess()
	{
		echo "<script type='text/javascript'>alert('Deze bestaat al');
											 window.location = 'index';
											 </script>";
	}
	//Als je klaar bent met datums kiezen en je op voltooien drukt, dan wordt deze functie aangeroepen.
	public function voltooiDate()
	{
		$this->database_model->voltooiDate($_SESSION['CurrentEventID']);
		redirect('index.php/Events/mainEvent');
	}
}    