<?php
class Location extends MY_AUTH{
    
    public function index(){
        $this->_initLocationVar($data);

        $data['InitLat'] = "52.086839";
        $data['InitLong'] = "5.175009";
        
        foreach ($this->database_model->getEvent($_SESSION['CurrentEventID']) as $row) 
        {
            if (!empty($row['Coordinaten']))
            {

                $remove = array("(", ")", " ");
                $coords = str_replace($remove, "", $row['Coordinaten']);
                
                $coordinaten = explode(",", $coords);
                
                $data['InitLat'] = $coordinaten[0];
                $data['InitLong'] = $coordinaten[1];
            }
        }

        $data['title'] = "Locatie";
        $this->load->view('header.php', $data);
        $this->load->view('menubalk.php');
        $this->load->view('locatiekiezer.php');
        $this->load->view('footer.php');
    }

    public function submit(){ 
	       
        if (empty($this->input->post('mapValue'))){   
            $_SESSION["eLatLngErr"] = ": Niets gekozen";
            redirect('index.php/Location');
        } 
		else{   
            $mapValue = $this->input->post('mapValue');
            $mapValueCoor = $this->input->post('mapValueCoor');
            $_SESSION["eLatLngErr"] = "";
            $_SESSION["eLatLng"] = $mapValue;
        }

        $eLatLng = $_SESSION["eLatLng"];
        $EventID = $_SESSION["CurrentEventID"];
        $UserID = $_SESSION["UserID"];

        $this->database_model->insertLocation($eLatLng, $EventID, $mapValueCoor);
        
        redirect('index.php/Events/mainEvent');
    }

    // Initialiseert de variabelen voor de view
    private function _initLocationVar(&$data){
        $this->projectw->_initVar($data, 'eLatLng');
        $this->projectw->_initVar($data, 'eLatLngErr');
    }
}