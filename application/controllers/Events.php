<?php
class Events extends MY_AUTH {

    // Load de Login page
    public function index(){
        redirect('index.php/Main/events');
    }

    public function overzicht(){
        $data['title'] = "Overzicht";
        $data['links'] = "";
        $this->_initEventVar($data);
				
        $UserID = $_SESSION["UserID"];
        $i = 0;
		
        foreach ($this->database_model->getEventByUserID($_SESSION["UserID"]) as $row){
            $feestnaam = $row['EventName'];
            $feestnaamurl = preg_replace("/[\s_]/", "-", $feestnaam);
            $feestid = $row['EventID'];
            $url = base_url("index.php/Events/loadSession/$feestid/$feestnaamurl");
			
            $data['links'] = $data['links'] . 
				"<div class='feestnaam'>
					<div class='overzicht_links'>
                		<form class='event_remove' action='https://www.projects.science.uu.nl/INFOB1PICA/2015/02/www/index.php/Events/verwijder/" . $feestid . "'>
            				<input type='button' onmouseover='veranderKleurV(this)' onmouseleave='veranderKleurVT(this)' class='verwijderknop_overzicht' onClick='verwijderEvent($i)' value=''>
            			</form>
            		</div>
					
            		<div class='overzicht_midden'>
                		<a class='overzicht_tekst' href=https://www.projects.science.uu.nl/INFOB1PICA/2015/02/www/index.php/Events/event_info/$feestid>
							<span class='spantest'>$feestnaam<br></span>
						</a>
            		</div>
					
           			<div class='overzicht_rechts'>
                		<form class='event_edit' action='$url'>
                			<input type='submit' onmouseover='veranderKleurB(this)' onmouseleave='veranderKleurBT(this)' class='bewerkknop_overzicht' value=''>
						</form>
					</div>
                </div>";
            
            $i = $i + 1;
        }

        $this->load->view('header.php', $data);
        $this->load->view('menubalk.php');
        $this->load->view('overzicht.php', $data);
        $this->load->view('footer.php');
    }

    
    public function gaNaarOpenEvent(){
        $data["ID"] = "";
        $feestid = $_SESSION['OpenEventID'];
        $gastmail = "";
        $UserID = $_SESSION['UserID'];
       
        $gastnaam = 0;
        $emailbezet = FALSE;
        
        foreach($this->database_model->getUserByID($UserID) as $row){
                $gastmail = $row['Email'];
                $gastnaam = $row['Voornaam'];    
        }
        
        foreach($this->database_model->getGuestByEmail($gastmail) as $row){
            if ($gastmail == $row['GastMail']){
                $emailbezet = TRUE;
            }
        }

        if ($emailbezet){
            $this->database_model->insertGuestEvent($feestid, $UserID, $gastnaam);
        } 
        else{   
            $this->database_model->insertGuest($gastnaam, $gastmail);
            $this->database_model->insertGuestEvent($feestid, $UserID, $gastnaam);
        }

        redirect('index.php/Main/events');
     
    }
    
    public function event_info($EventID)
    {  
        $data['gast'] = "";
        $data['costs'] = "";
        $data['totaal'] = "";
        
        $this->_checkOwner($EventID);
        
        $_SESSION['CurrentEventID'] = $EventID;

        for ($i = 1; $i <= 4; $i++)
        {
            $data['final' . $i] = "";
            $data['gdata' . $i] = "";
            $data['tdata' . $i] = "";    
            ${"gdata" . $i} = "";
        }

        foreach($this->database_model->getEvent($EventID) as $row)
        {
            $data['beschrijving'] = $row['Beschrijving'];
            $data['locatie'] = $row['Locatie'];
            $OEvent = $row['OpenEvenement'];
        }
        
        foreach($this->database_model->getEventNameHost($EventID) as $row)
        {
            $data['EventNaam'] = $row['EventName'];
        }
     
        foreach($this->database_model->getDataByEventID($EventID) as $row)
        {
            for ($i = 1; $i <= 4; $i++)
            {
                ${"gdata" . $i} = $row['Data' . $i];
                
                if (${"gdata" . $i} != 0)
                {
                    ${"gdata" . $i . "Datum"} = date_create_from_format('U', 
                        ${"gdata" . $i} + 3600);
                    $data['gdata' . $i] = ${"gdata" . $i . "Datum"}->format('d F Y');
                }
                else if (${"gdata" . $i} == 0)
                {
                    $data['gdata' . $i] = " ";
                }    
            }
        }

        foreach ($this->database_model->getGuestEvent($EventID) as $row)
        {
            $gastid = $row['GastID'];
            $gastAanwezig = $row['Aanwezig'];
            $gastNaam = $row['GastNaam'];
            
            if ($row['Aanwezig'] == 1)
            {
                $gastAanwezig = "Van de partij!";
            }
            else if ($row['Aanwezig'] == 0)
            {
                $gastAanwezig = "Nog niet gereageerd.";
            }
            else if ($row['Aanwezig'] == 2)
            {
                $gastAanwezig = "Sjaak Afhaak :(";
            }

            for ($i = 1; $i <= 4; $i++)
            {
                ${"D" . $i} = $row['Datum' . $i];
                
                if(${"D" . $i} == 1)
                {
                    $data['tdata' . $i] =  $data['tdata' . $i]+1;
                }

                if(${"D" . $i} == 1)
                {
                    ${"D" . $i} = "Ja";
                }
                else
                {
                    ${"D" . $i} = "Nee";
                }
            }

            foreach($this->database_model->getGuestByID($gastid) as $gastrow)
            {
               
                $gastMail = $gastrow['GastMail'];
                
                for ($i = 1; $i <= 4; $i++)
                {
                    if (${"gdata" . $i} != 0) 
                    {
                        ${"Gdatumstr" . $i} = "<div class ='rechtercell_eventinfo'>
                                " . ${"D" . $i} . "
                            </div>";
                            $data['final' . $i] = "
                                <div class='rechtercell_eventinfo'> 
                                    <form class='finalform' method='post' action='https://www.projects.science.uu.nl/INFOB1PICA/2015/02/www/index.php/Events/FinalDate/" . $i . "'>
                                        <input type='submit' class='verzendknop' id='kiesDatum' name='commit' value='Kies Datum' onClick='maakFinal(" . ($i-1) . ")'>
                                    </form>
                                </div>";
                    }
                    else 
                    {
                        ${"Gdatumstr" . $i} = "";
                        $data['tdata' . $i] = "";
                        $data['final' . $i] = "";
                    }
                }

                $data['gast'] = $data['gast'] . "
                    <div class='tabelrij_gasten2'>
                        <div class='linkercell_gasten'>
                            " . $gastNaam . "
                        </div>
                        <div class ='rechtercell_gasten'>
                            " . $gastAanwezig . "
                        </div>
                        <div class ='rechtercell_eventinfo'>
                            " . $Gdatumstr1 . "
                        </div>
                        <div class ='rechtercell_eventinfo'>
                            " . $Gdatumstr2 . "
                        </div>
                        <div class ='rechtercell_eventinfo'>
                            " . $Gdatumstr3 . "
                        </div>
                        <div class ='rechtercell_eventinfo'>
                            " . $Gdatumstr4 . "
                        </div>
                    </div>
                ";
            }
        }
        
        $totaal = "";
        foreach ($this->database_model->getCost($EventID) as $row)
        {
            $costNaam = $row['KostNaam'];
            $costPrice = $row['KostPrijs'];
            $totaal = $totaal + $costPrice;
            $data['costs'] = $data['costs'] . "
            	<div class='tabelrij_gasten2'>
                	<div class='linkercell_kosten'>
						" . $costNaam . "
                	</div>
                	<div class ='rechtercell_kosten'>
						€" . $costPrice . "
                	</div>
            	</div>
            ";
        }
		
		$data['totaal'] = "<p> Totaal: € " .$totaal."</p>";
        $EventNaam = "" . $data['EventNaam'];
		$EventNaamE = str_replace("'", "-test-", $EventNaam);
        $UserID = $_SESSION["UserID"];
        $url = base_url("index.php/Events/loadSession/$EventID/$EventNaamE");
        $data['url'] = $url;
        $data['title'] = "Event Informatie";
        $this->load->view('header.php', $data);
        $this->load->view('menubalk.php');
        $this->load->view('eventInfo.php', $data);
        $this->load->view('footer.php'); 
    }
    
    public function uitgenodigd_voor()
    {
        $data['title'] = 'Uitnodigingen';
        $data['uitnod'] = "";
        $gastID="";

        // Als de gebruiker nog geen gastid heeft
        foreach($this->database_model->getEmptyGastID() as $row){
            $userEmail = $row['Email']; 
            
            foreach($this->database_model->getGEmail($userEmail) as $Frow){
                $gastID = $Frow['GastID'];
                $this->database_model->setGastID($userEmail, $gastID);
            }
        }
        
        // Als de gebruiker wel een gastid heeft
        foreach($this->database_model->getNameHost($_SESSION['UserID']) as $row)
        {
            $userEmail = $row['Email'];

            foreach($this->database_model->getGEmail($userEmail) as $Frow){
                $gastID = $Frow['GastID'];
            }
        }        
        
        $UserID = $_SESSION["UserID"];
        $i = 0;
        
        foreach ($this->database_model->getEventIDByGastID($gastID) as $row){
            $EventID = $row['EventID'];

            $key = md5($gastID . $this->projectw->_key());

            foreach($this->database_model->getUserbyEvent($EventID)as $row)
            {
                $GebruikerID = $row['UserID'];

                foreach($this->database_model->getNameHost($GebruikerID) as $row2)
                {
                    $naam = $row2['Voornaam'] . ' ' . $row2['Achternaam'];
                }
            }
            
            foreach ($this->database_model->getEventNameNonDeleted($EventID)as $row2){
                
                $feestnaam = $row2['EventName'];

              
                    $url = "https://www.projects.science.uu.nl/INFOB1PICA/2015/02/www/index.php/Guestlist/landing/$EventID/$gastID/$key";

                    $data['uitnod'] = $data['uitnod'] . "
						<div class='feestnaam'>
                            <div class='uitgenodigd_links'>$naam
                    		</div>
                    		<div class='uitgenodigd_rechts'>$feestnaam
                    		</div>
                            <a href=$url>
                                <span class='spantest'><br></span>
                            </a>
						</div>
                    ";
                        
                    $i = $i + 1;      
             
            }
            }

            
        
        
        $this->load->view('header.php', $data);
        $this->load->view('menubalk.php');
        $this->load->view('uitgenodigdVoor.php', $data);
        $this->load->view('footer.php'); 
    }
    
    public function verwijder($EventID){ 
        $this->database_model->removeEvent($EventID);
        $this->database_model->setDeleted($EventID);
        redirect('index.php/Events/overzicht');
    }
    
    public function submit($EventID){
        // Open Event
        if ($this->input->post('OEvent')){
            $OEvent = $this->input->post('OEvent');
        }
        else{
            $OEvent = 0;
        }

        $this->database_model->setOpenEvent($_SESSION['CurrentEventID'], $OEvent);
        
        // Beschrijving van evenement
        
        $beschrijving_tekst = "";
        $beschrijving_tekst =  $this->input->post("eventBeschrijving");  
        $_SESSION["beschrijving_tekst"] = $beschrijving_tekst;
        $beschrijving_tekst = htmlentities($beschrijving_tekst, ENT_QUOTES);
        //$beschrijving_tekst = sqlite_escape_string($beschrijving_tekst);
        $this->database_model->insertBeschrijving($_SESSION["CurrentEventID"], $beschrijving_tekst);
        redirect('index.php/Events/mainEvent');
    }

	public function checkMail($gastid, $EventID, $ToMail){
    	$this->database_model->setNieuwMail($gastid, $EventID, $ToMail);
    	redirect('index.php/Events/verzendcheck');
	}
    
	public function verzendcheck(){
    	$data['gast'] = '';
    	$EventID = $_SESSION["CurrentEventID"];
    
    foreach ($this->database_model->getGuestEvent($_SESSION["CurrentEventID"]) as $row){
        $gastid = $row['GastID'];
        $gastAanwezig = $row['Aanwezig'];
        $gastNaam = $row['GastNaam'];

        foreach($this->database_model->getGuestByID($gastid) as $gastrow){

            $gastMail = $gastrow['GastMail'];
            
            if ($row['NieuwMail'] == 0){
                $data['gast'] = $data['gast'] . "
                	<div class='tabelrij' id='padding_tabelrij'>
					
                    	<div class='inputbox guestcell_links'>
							" . $gastNaam . "
                    	</div>
						
                    	<div class ='inputbox guestcell_midden'>
							" . $gastMail . "
                    	</div>
						
                    	<div class ='inputbox guestcell_rechts'>
                        	<form class='form_verzendcheck' action=' https://www.projects.science.uu.nl/INFOB1PICA/2015/02/www/index.php/Events/checkMail/$gastid/$EventID/1'>
                            	<input style='margin-top: 0px;' type='submit' class='verzendknop'  value='Niet uitnodigen'>
                        	</form>
                    	</div>
                	</div>
				";
            } 
            else{
                $data['gast'] = $data['gast'] . "
                	<div class='tabelrij' id='padding_tabelrij'>
                    	<div class='inputbox guestcell_links'>
							" . $gastNaam . "
                    	</div>
                    	<div class ='inputbox guestcell_midden'>
							" . $gastMail . "
                    	</div>
                    	<div class ='inputbox guestcell_rechts'>
                        	<form class='form_verzendcheck' action=' https://www.projects.science.uu.nl/INFOB1PICA/2015/02/www/index.php/Events/checkMail/$gastid/$EventID/0'>
                            	<input style='margin-top: 0px;' type='submit' class='verzendknop' value='Wel uitnodigen'>
                        	</form>
                    	</div>
                	</div>
				";    
            }
		}
	}    

    // Beschrijving van evenement

    $beschrijving_tekst = "";	
	
    $beschrijving_tekst =  $this->input->post("eventBeschrijving");  
    $_SESSION["beschrijving_tekst"] = $beschrijving_tekst;
    $beschrijving_tekst = htmlentities($beschrijving_tekst, ENT_QUOTES);
	
    //$beschrijving_tekst = sqlite_escape_string($beschrijving_tekst);
    $this->database_model->insertBeschrijving($_SESSION["CurrentEventID"], $beschrijving_tekst);
    $data['beschrijving'] = $beschrijving_tekst;

    // Open Event
	
    if ($this->input->post('OEvent')){
        $OEvent = $this->input->post('OEvent');

        foreach ($this->database_model->getEvent($_SESSION['CurrentEventID']) as $row){
            if ($row['DatumVoltooid'] == 0 || $row['LocatieVoltooid'] == 0 || !isset($beschrijving_tekst) || trim($beschrijving_tekst)===''){
                $_SESSION['OpenEvenementErr'] = 'Er moet een datum, locatie en beschrijving gegeven worden voor een open evenement';
                redirect('index.php/Events/mainEvent');
            }
            else{
                $_SESSION['OpenEvenementErr'] = '';
            }
        }

    }
    else{
        $OEvent = 0;
    }

    $this->database_model->setOpenEvent($_SESSION['CurrentEventID'], $OEvent);

    $data['title'] = "Controle";
    $this->load->view('header.php', $data);
    $this->load->view('menubalk.php');
    $this->load->view('verzendcheck.php', $data);
    $this->load->view('footer.php');
	}
    
    public function verzendmails(){
        $gastid = 0;
        $eventid = $_SESSION['CurrentEventID'];
		
        foreach ($this->database_model->getEventNameHost($eventid) as $row){
            $UserID = $row['UserID'];
            $EventName = $row['EventName'];
               }
        foreach ($this->database_model->getUserByID($UserID) as $row){
            $Voornaam = $row['Voornaam'];
            $Achternaam = $row['Achternaam']; 
        }
                
        foreach ($this->database_model->getGastID($eventid) as $idrow){
            $gastid = $idrow['GastID'];
            $gastnaam = $idrow['GastNaam'];
            $NieuwMailBool = $idrow['NieuwMail'];
			
            foreach ($this->database_model->getgastmail($gastid) as $id2row){
                $gastmail = $id2row['GastMail'];
                
				if($NieuwMailBool == 1){
                	$this->sendinvitemail($gastid, $gastmail, $gastnaam, $eventid, $EventName, $Voornaam, $Achternaam);
                }
            }
        }
		redirect("index.php/Events/event_info/$eventid");
	}
      
    public function sendinvitemail($gastid, $mail, $gastnaam, $eventid, $EventName, $Voornaam, $Achternaam){
        $key = md5($gastid . $this->projectw->_key());

        $url = "<html><a href='https://www.projects.science.uu.nl/INFOB1PICA/2015/02/www/index.php/Guestlist/landing/$eventid/$gastid/$key'>HIER</a></html>";
		
		$url2 = "<html><a href='https://www.projects.science.uu.nl/INFOB1PICA/2015/02/www'>Project W</a></html>";
     
        $message = "<style type='text/css'>
        	@media only screen and (max-width: 620px) {
				.header{
					
				}
				
				.text{
					
				}
				
				.sidebar{
					
				}
			}
			
			.header{
				width:300px; 
				height:188px;
			}
			
			.text{
				width:1500px;
			}
			
			.sidebar{
				 width:300px;
				 height:400px;
			}
			
        </style>
    
    	<table cellspacing='0' cellpadding='10' border='0' width='100%' style='background-color:#FC6;'>
    		<tr>
            	<td><img class='header' src='https://www.projects.science.uu.nl/INFOB1PICA/2015/02/www/images/logo_big.png' alt='Project W'></td>
            </tr>
            <tr>
                <td style='background-color:#000000;' width='0' cellpadding='0'><img class='sidebar' src='https://www.projects.science.uu.nl/INFOB1PICA/2015/02/www/images/mail_background.PNG' alt='Project W'></td>
            	<td style='background-color:#FC6;' class='text'>
                <p style='font-family: Roboto Slab; font-size: 30px;'>
                	Hallo ".$gastnaam.",</p><br>
                <p style='font-family: Roboto Slab; font-size: 25px;'>
                    Jij bent uitgenodigd door ".$Voornaam . " " .$Achternaam." voor een geweldig event! 
                    Als je naar het evenement \"".$EventName."\" wilt gaan, klik dan " .$url." voor de uitnodiging.<br><br>
                    Veel plezier!</p><br><br>
                <p style=' font-size: 10px;'>
                	Dit evenement is mede mogelijk gemaakt door ".$url2.", dé website om al uw evenementen mee te plannen!</p>
                </td>
            </tr>
    	</table>
        
        
        
	</body>
</html>";

        $headers =  'From: ProjectW@uu.nl' . "\r\n" .
              'Reply-To: noreply.projectw@gmail.com' . "\r\n" .
              'Content-type: text/html; charset=iso-8859-1' . "\r\n".
              'X-Mailer: PHP/' . phpversion();
        mail($mail, 'Uitgenodigd voor event ' .$EventName, $message, $headers);
    }

    public function nieuw(){
        $data['title'] = "Nieuw";
        $this->_initEventVar($data);
        $this->load->view('header.php', $data);
        $this->load->view('menubalk.php');
        $this->load->view('nieuw.php', $data);
        $this->load->view('footer.php');
    }

    public function mainEvent(){
        $data['title'] = "Event";
        $data['checked'] = "";

        foreach ($this->database_model->getEvent($_SESSION['CurrentEventID']) as $row){
            
			if ($row['OpenEvenement'] == 1){
                $data['checked'] = "checked='checked'";
            }
        }
        
        //voorwaarde checkmarks 
        $this->_initEventVar($data);
        $this->buttonCheckmarkSwitch("DatumVoltooid", "DatumBool", $data);
        $this->buttonCheckmarkSwitch("LocatieVoltooid", "LocatieBool", $data);
        $this->buttonCheckmarkSwitch("GastenVoltooid", "GastenBool", $data);
		$this->buttonCheckmarkSwitch("KostenVoltooid", "KostenBool", $data);
        $data['DatumBoolText'] = $this->buttonTextSwitch("DatumBool");
        $data['LocatieBoolText'] = $this->buttonTextSwitch("LocatieBool");
        $data['GastenBoolText'] = $this->buttonTextSwitch("GastenBool");
		$data['KostenBoolText'] = $this->buttonTextSwitch("KostenBool");
        $this->load->view('header.php', $data);
        $this->load->view('menubalk.php');
        $this->load->view('mainevent.php', $data);
        $this->load->view('footer.php');
    }

    public function buttonSwitch($button){
        $this->database_model->updateEventBool($_SESSION["CurrentEventID"], $button);
        
		foreach ($this->database_model->getEvent($_SESSION["CurrentEventID"]) as $row){
            $_SESSION["DatumBool"] = $row['DatumBool'];
            $_SESSION["LocatieBool"] = $row['LocatieBool'];
            $_SESSION["GastenBool"] = $row['GastenBool'];
			$_SESSION["KostenBool"] = $row['KostenBool'];
        }
		
        redirect('index.php/Events/mainEvent');
    }

    public function buttonTextSwitch($button){
        
		if ($_SESSION["$button"] == 1){
            return "Aan";
        } 
		else{
            return "Uit";
        }
    }

   public function sendFinalmail($gastid, $gastmail, $gastnaam, $eventid, $Voornaam, $Achternaam, $dataToSend, $EventName, $Adres){
        
       $key = md5($gastid . $this->projectw->_key());

        $url = "<html><a href='https://www.projects.science.uu.nl/INFOB1PICA/2015/02/www/index.php/Guestlist/landing/$eventid/$gastid/$key'>HIER</a></html>";
		
		$url2 = "<html><a href='https://www.projects.science.uu.nl/INFOB1PICA/2015/02/www'>Project W</a></html>";
     
        $message = "<p style='font-family: Roboto Slab; font-size: 30px;'>Hallo ".$gastnaam.",</p><br><p style='font-family: Roboto Slab; font-size: 25px;'>De definitieve datum voor het evenement \"".$EventName."\" van ".$Voornaam." ".$Achternaam." is gekozen! Het is ".$dataToSend." geworden, op de locatie ".$Adres.". Als je al de dag bevestigd hebt, kun je dit als een bevestigingsmail zien, anders kan je ".$url." bevestigen of je komt of niet.<br>Veel plezier namens Project W!</p><br><br><p style=' font-size: 10px;'>Dit evenement is mede mogelijk gemaakt door ".$url2.", dé website om al uw evenementen mee te plannen!</p>";

        $headers =  'From: ProjectW@uu.nl' . "\r\n" .
              'Reply-To: noreply.projectw@gmail.com' . "\r\n" .
              'Content-type: text/html; charset=iso-8859-1' . "\r\n".
              'X-Mailer: PHP/' . phpversion();
        mail($gastmail, 'bevestiging van event', $message, $headers);
    }
    
    public function FinalDate($num){
        
        $EventID = $_SESSION['CurrentEventID'];
        //in database final is 1 zetten
        
        $this->database_model->setFinal($EventID, $num);
        //functie voor het sturen van mails
        
         foreach ($this->database_model->getEventNameHost($EventID) as $row){
            $UserID = $row['UserID'];
            $EventName = $row['EventName'];
               }
        foreach ($this->database_model->getUserByID($UserID) as $row){
            $Voornaam = $row['Voornaam'];
            $Achternaam = $row['Achternaam']; 
        }
        
        foreach($this->database_model->getDataByEventID($EventID) as $row){
            $datum = $row['Data'.$num];
        
         if ($datum != 0){
                $dataDatum = date_create_from_format('U', $datum + 3600);
                $dataToSend = $dataDatum->format('d F Y');
            }
        }
        foreach ($this->database_model->getEvent($EventID) as $row){
            $Adres = $row['Locatie'];
        }
        foreach ($this->database_model->getGastID($EventID) as $idrow){
            $gastid = $idrow['GastID'];
            $gastnaam = $idrow['GastNaam'];
           		
            foreach ($this->database_model->getgastmail($gastid) as $id2row){
                $gastmail = $id2row['GastMail'];
                $this->sendFinalmail($gastid, $gastmail, $gastnaam, $EventID, $Voornaam, $Achternaam, $dataToSend, $EventName, $Adres);
                
            }
        }
        redirect('index.php/Main/events');
    }
	
   
    
    
    public function buttonCheckmarkSwitch($button, $checkmarkbool, &$data){
		
        foreach ($this->database_model->getVoltooidKnoppen($_SESSION["CurrentEventID"]) as $row){
            $result = $row[$button];
        }
        
		foreach ($this->database_model->getBool($_SESSION["CurrentEventID"]) as $row){
            $result2 = $row[$checkmarkbool];
        }
		
        $buttonwaarde = $button . "waarde";
        $imgcross = base_url('images/cross.png');
        $imgcheck = base_url('images/checkmark.png');
		
        if ($result == 1 || $result2 == 0){
            $data["$button"] = "<img class='check' src=$imgcheck />";
			$data["$buttonwaarde"] = 1;
        } 
		
		else{
			$data["$button"] = "<img class='check' src=$imgcross />";
			$data["$buttonwaarde"] = 0;     
        }
    }

    public function nieuwEvent(){
        // define variables and set to empty values
		
        if (empty($this->input->post('EventName'))){
            $_SESSION["newEventErr"] = "Kan niet leeg zijn.";
            redirect('index.php/Events/nieuw');
            return;
        } 
		else{
            $eventname = $this->input->post('EventName');
            $_SESSION["newEventErr"] = "";
            $_SESSION["eventname"] = $eventname;
            $UserID = $_SESSION["UserID"];

            $this->database_model->insertEventsUser($eventname, $UserID);

            foreach ($this->database_model->getEventsUser($eventname, $UserID) as $row){
                $result = $row['EventID'];
            }

            $_SESSION["CurrentEventID"] = $result;
            $EventID = $_SESSION["CurrentEventID"];
            $this->database_model->insertEvent($EventID);

            redirect('index.php/Events/overzicht');
            //$this->loadSession($EventID);
        }
    }

    public function loadSession()
    {
        $EventID = $this->uri->segment(3);
        $EventName = str_replace("-test-", "'", $this->uri->segment(4));
		$EventName = urldecode($EventName);
        $EventNameUrl = str_replace("-", " ", $EventName);

        // Check of de event eigendom van de gebruiker is
        $this->_checkOwner($EventID);

        $_SESSION["CurrentEventName"] = $EventNameUrl;
        $_SESSION["CurrentEventID"] = $EventID;
        foreach ($this->database_model->getEvent($EventID) as $row){
            $_SESSION["DatumBool"] = $row['DatumBool'];
            $_SESSION["DatumID"] = $row['DatumID'];
            $_SESSION["LocatieBool"] = $row['LocatieBool'];
            $_SESSION["LocatieID"] = $row['LocatieID'];
            $_SESSION["GastenBool"] = $row['GastenBool'];
            $_SESSION["GastenID"] = $row['GastenID'];
			$_SESSION["KostenBool"] = $row['KostenBool'];
            $_SESSION["KostenID"] = $row['KostenID'];
			$_SESSION["beschrijving_tekst"] = $row['Beschrijving'];
        }
        redirect('index.php/Events/mainEvent');
    }

    // Check of de event eigendom van de gebruiker is
    private function _checkOwner($EventID)
    {	
        foreach ($this->database_model->getEventNameHost($EventID) as $row){
			
            if ($row['UserID'] !== $_SESSION['UserID']){
                redirect('index.php/Events/overzicht');
            }
        }
    }

    // Initialiseert de variabelen voor de view
    private function _initEventVar(&$data){
        $this->projectw->_initVar($data, 'newEventErr');
        $this->projectw->_initVar($data, 'DatumBool');
        $this->projectw->_initVar($data, 'LocatieBool');
        $this->projectw->_initVar($data, 'GastenBool');
		$this->projectw->_initVar($data, 'KostenBool');
        $this->projectw->_initVar($data, 'OpenEvenementErr');
    }
}
