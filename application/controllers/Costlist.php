<?php
class Costlist extends MY_AUTH{
    
    public function index(){
        $data['title'] = "Kostenlijst";
        $this->_initCostVars($data);
        $data['costs'] = '';
        $data['totaal'] = 0;
        $i = 0;
		
        foreach ($this->database_model->getCost($_SESSION["CurrentEventID"]) as $row){
	        $costNaam = $row['KostNaam'];
	        $costPrice = $row['KostPrijs'];
			$costID = $row['KostenID'];
            $data['totaal'] = $data['totaal'] + $costPrice;
            
            $data['costs'] = $data['costs'] . "
            	<div class='tabelrij'>
               		<div class ='linkerlinkercel'>
                    	<form class='cost_remove' action='https://www.projects.science.uu.nl/INFOB1PICA/2015/02/www/index.php/Costlist/remove/" . $costID . "'>
							<input type='button' class='verwijderknop' onClick='verwijderKosten(" . $i . ")' value=''>
                    	</form>
                	</div>
                	<div class='inputbox linkercell'>
						" . $costNaam . "
                	</div>
                	<div class ='inputbox middencell'>
						€" . $costPrice . "
                	</div>
            	</div>
            ";
            $i = $i + 1;
        }

        $this->load->view('header.php', $data);
        $this->load->view('menubalk.php');
        $this->load->view('kostenlijst.php', $data);
        $this->load->view('footer.php');
    }

    public function remove($costID){
        $this->database_model->removeCost($_SESSION['CurrentEventID'], $costID);
        redirect('index.php/Costlist');
    }

    public function toevoegen(){
        
		if (empty($this->input->post("naam"))){
            $_SESSION["kostNaamErr"] = "Vul een beschrijving van de kosten in";
            $_SESSION["kostNaam"] = "";
            
            if (empty($this->input->post("prijs"))){
                $_SESSION["prijsErr"] = "Vul kosten in";
                redirect('index.php/Costlist');
            } 
			else{   
                $prijs = $this->input->post("prijs");
                $_SESSION["prijsErr"] = "";
                $_SESSION["prijs"] = $prijs;
            }
        redirect('index.php/Costlist');
        } 
		else{
            $kostNaam = $this->input->post("naam");
            $_SESSION["kostNaamErr"] = "";
            $_SESSION["kostNaam"] = $kostNaam;

            if (empty($this->input->post("prijs"))){
                $_SESSION["prijsErr"] = "Vul kosten in";
                redirect('index.php/Costlist');
            } 
			else{   
                $prijs = $this->input->post("prijs");
                $_SESSION["prijsErr"] = "";
                $_SESSION["prijs"] = $prijs;
            }
        }
        
        $this->database_model->insertCost($kostNaam, $prijs, $_SESSION["CurrentEventID"]);
        $this->_resetCostVar();
        redirect('index.php/Costlist');
    }

    public function voltooid(){
        $this->database_model->updateCostVoltooid($_SESSION["CurrentEventID"]);
        redirect('index.php/Events/mainEvent');
    }

    public function _initCostVars(&$data){
        $this->projectw->_initVar($data, 'kostNaam');
        $this->projectw->_initVar($data, 'prijs');
		$this->projectw->_initVar($data, 'betaler');
        $this->projectw->_initVar($data, 'kostNaamErr');
        $this->projectw->_initVar($data, 'prijsErr');
		$this->projectw->_initVar($data, 'betalerErr');
    }

    private function _resetCostVar(){
        $this->projectw->_resetVar('kostNaam');
        $this->projectw->_resetVar('kostNaamErr');
        $this->projectw->_resetVar('prijs');
        $this->projectw->_resetVar('prijsErr');
		$this->projectw->_resetVar('betaler');
        $this->projectw->_resetVar('betalerErr');
    }
}
?>