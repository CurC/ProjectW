<?php
class Openevents extends MY_Controller {

    // Load de Login page
    public function index(){
        $data['title'] = "Alle Events";
        $data['OEvents'] = "";
		
		$long = $this->input->post('lon');
		$lat = $this->input->post('lat');
		//$_SESSION['latit'] = $lat;
		
        foreach ($this->database_model->getOpenEvents() as $row){
            $feestid = $row['EventID'];
            $feestloc = $row['Locatie'];
			$coords = $row['Coordinaten'];

            foreach($this->database_model->getEventNameHost($feestid) as $Frow){
                $feestnaam = $Frow['EventName'];
            }

            foreach($this->database_model->getDataByEventID($feestid) as $Frow){
                $data1 = $Frow['Data1'];

                $date = date_create_from_format('U', $data1 + 3600);
                $feestdatum = $date->format('d F Y');
            }

            if ($data1 < time() - 50400){
                continue;
            }
			
			if (isset($long)){
				
				if ($this->inDeBuurt($coords, $lat, $long, $data) == false){
					continue;
				}
				
				$data['infoBuurt'] = "Deze evenementen zijn bij jou in de buurt in een straal van ~7km";
			}

            $data['OEvents'] = $data['OEvents'] . "
				<div class='feestnaam'>
				
            		<div class='open_event_links'>
						" . $feestnaam . "
            		</div>
					
            		<div class='open_event_midden'>
						$feestloc
					</div>
					
            		<div class='open_event_rechts'>
						$feestdatum
					</div>
					
            		<a href=https://www.projects.science.uu.nl/INFOB1PICA/2015/02/www/index.php/Openevents/event_info/$feestid/$feestnaam>
						<span class='spantest'><br></span>
					</a>
				</div>
			";
        }
         
        $this->load->view('header.php', $data);
        $this->load->view('menubalk.php');
        $this->load->view('all_events.php', $data);
        $this->load->view('footer.php');
    }

    public function event_info($EventID){  
        $data['gast'] = "";
        $_SESSION['OpenEventID'] = $EventID;
		
        // Check of het evenement wel open is
        foreach ($this->database_model->getEvent($EventID) as $row){
			
            if ($row['OpenEvenement'] == 0){
                redirect('index.php/Main/all_events');
            }
        }

    	foreach ($this->database_model->getEvent($EventID) as $row){
         	$data['beschrijving'] = $row['Beschrijving'];
            $data['locatie'] = $row['Locatie'];
     	}
        
        foreach ($this->database_model->getEventNameHost($EventID) as $row){
            $data['EventNaam'] = $row['EventName'];
        }
   
        foreach ($this->database_model->getGuestEvent($EventID) as $row){
                $gastNaam = $row['GastNaam']; 
            
                $data['gast'] = $data['gast'] . "
                <div class='tabelrij' id='openevent_tabelrij'>
                    <div class='linkercell'>
						" . $gastNaam . "
                    </div>
                </div>";
            
        }
        
        $data['title'] = "Open Event Informatie";
        $this->load->view('header.php', $data);
        $this->load->view('menubalk.php');
        $this->load->view('openeventInfo.php', $data);
        $this->load->view('footer.php'); 
    }
	
	private function inDeBuurt($coords, $lat, $long, &$data){
		//$_SESSION['latit'] = $lat;
		//$data['latit'] = "";
		//$_SESSION['latit'] = "hoi";
		
		$weghalen = array("(", ")", " ");
		$coords = str_replace($weghalen, "", $coords);
		
		$coordinaten = explode(",", $coords);
		$_SESSION['latit'] = ($lat - $coordinaten[0] < 0.1 && $lat - $coordinaten[0] > -0.1);
			
		$latd = abs($lat - $coordinaten[0]);
		$longd = abs($long - $coordinaten[1]);
            
		if ($latd < 0.1 || $longd < 0.1){
			//$data['check'] = 1;
            //$data['latit'] = $coordinaten[0];
			return true;
		}
        //$data['check'] = 1;
        //$data['latit'] = $coordinaten[0];
		return false;	
	}
}