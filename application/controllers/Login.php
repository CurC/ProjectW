<?php

class Login extends MY_Controller {
    
    // Load de Login page
    public function index() 
    {
        $data['title'] = "Login";
        $this->_initLoginVar($data);        
        $this->load->view('header.php', $data);
        $this->load->view('menubalk.php');
        $this->load->view('login.php', $data);
        $this->load->view('footer.php');
    }
    
    public function account(){
		foreach($this->database_model->getUserById($_SESSION["UserID"]) as $row){
			$naam = $row['Voornaam'] . " " . $row['Achternaam'];
            $email = $row['Email'];
            $geboortedatum = $row['GeboorteDatum'];
		}
		$data['naam'] = $naam;
        $data['email'] = '<p>' . $email . '</p>';
        $data['geboortedatum'] = '<p>' . $geboortedatum . '</p>';
        $data['title'] = "Account";
        $this->load->view('header.php', $data);
        $this->load->view('menubalk.php');
        $this->load->view('account.php');
        $this->load->view('footer.php');    
    }
    
    public function logUit() 
    {

        if ($this->input->cookie('Remember'))
        {
            $this->input->set_cookie('Remember', 0, -999999);
        }
        
        if ($this->input->cookie('UID'))
        {
            $this->input->set_cookie('UID', 0, -999999);
        }

        $this->database_model->updateCookieData($_SESSION['UserID'], "");

        // Remove all session variables
        session_unset(); 

        // Destroy the session 
        session_destroy();

        redirect('index.php/Main');
    }

    public function sessionKill() 
    {
        // Remove all session variables
        session_unset(); 

        // Destroy the session 
        session_destroy(); 

        redirect('index.php/Main');
    }

    public function wwchange()
    {
         if ($_SESSION['ingelogd'] != true){
            redirect('index.php/Login');
        }

        $this->_initPassChangeVar($data);
        $data['title'] = "Verander Wachtwoord";
        $this->load->view('header.php', $data);
        $this->load->view('menubalk.php');
        $this->load->view('wachtwoordchange.php', $data);
        $this->load->view('footer.php');
    }

    public function passchange()
    {
    $this->load->model('database_model');

        // Kijkt of de velden leeg zijn
        if (empty($this->input->post('password'))) 
        {
            $_SESSION['passErr'] = "Typ een nieuw wachtwoord in.";
            redirect('index.php/Login/wwchange');
        } 
        else if (strlen($this->input->post('password'))<7) 
        {
            $_SESSION['passErr'] = "Wachtwoord moet minimaal 7 tekens bevatten."; 
            redirect('index.php/Login/wwchange');
        } 
        else 
        {
            $pass1 = $this->input->post('password');
           $_SESSION['passErr'] = "";  
        }

        if (($this->input->post('password'))!=($this->input->post('checkpassword'))) 
        {
            $_SESSION['passErr2'] = "De wachtwoorden komen niet overeen";
            redirect('index.php/Login/wwchange');
        } 
        else 
        {
            $pass2 = $this->input->post('checkpassword');
           $_SESSION['passErr2'] = "";  
        }

        $passhash = password_hash($pass1, PASSWORD_DEFAULT);
        $this->database_model->changepass($_SESSION['UserID'], $passhash);
        redirect('index.php/Login');
    }
    
    // Kijkt of de logininfo goed is
    public function checklogin() 
    {
        // Kijkt of de velden leeg zijn
        if (empty($this->input->post('login'))) 
        {
            $_SESSION["emailErr"] = "Vul je emailadres in";
            redirect('index.php/Login');
        } 
        else 
        {
            $user = $this->input->post('login');
            $_SESSION["emailErr"] = "";
            $_SESSION["email"] = "$user";   
        }

        if (empty($this->input->post('password'))) 
        {   
            $_SESSION["passErr"] = "Er is een wachtwoord nodig";
            redirect('index.php/Login');
            return;
        } 
        else 
        {   
            $pass = $this->input->post('password');
            $_SESSION["passErr"] = "";
        }
        
        // Pakt de userid en password uit de database
        foreach ($this->database_model->getUser($user) as $row) 
        {
            $result = $row['PassHash'];
            $dbUserID = $row['UserID'];
            $UserActief = $row['Actief'];
        }
        
        // Kijkt of de password gelijk is aan die in de database
        if (password_verify($pass, $result) && $UserActief == 1)
        {
            echo 'password klopt';
            $_SESSION["ingelogd"] = TRUE;
            $_SESSION["UserID"] = "$dbUserID";

            // Set Cookie voor de Remember Me functie
            if ($this->input->post('remember_me')) 
            {
                $ip = md5($this->input->ip_address());
                set_cookie('Remember', 'TRUE', time()+999999);
                set_cookie('UID', "$dbUserID", time()+999999);
                $this->database_model->updateCookieData($dbUserID, $ip);
            }

            redirect('index.php/Main/events');
        }
        else if($UserActief == 0)
        {
            $_SESSION["activatieErr"] = "Activeer uw account eerst door op de link te klikken in de activatiemail.";
            redirect('index.php/Login');
        }
        else 
        {
            $_SESSION["passErr"] = "Dit wachtwoord is niet het goede wachtwoord";
            redirect('index.php/Login');
        }
    }
    
    public function registratie() 
    {
        $data['title'] = "Registratie";
        $this->_initLoginVar($data);
        $this->load->view('header.php', $data);
        $this->load->view('menubalk.php');
        $this->load->view('registreer.php', $data);
        $this->load->view('footer.php');
    }

    public function veranderpass(){
   
        $this->_initLoginVar($data);
        $this->load->view('header.php', $data);
        $this->load->view('menubalk.php');
        $this->load->view('veranderpass.php', $data);
        $this->load->view('footer.php');
    }
    
    public function verzendpassmail()
    {          
        $mail= $_POST['verander'];
        $digits = 6;
        $newpass = rand(pow(10, $digits-1), pow(10, $digits)-1);
        $newpasshash = password_hash($newpass, PASSWORD_DEFAULT);;
        $this->database_model->resetpass($mail, $newpasshash);
        
        $url = "<html><a href='https://www.projects.science.uu.nl/INFOB1PICA/2015/02/www/index.php/Login'>Klik hier om in te loggen.</a></html>";
        
        $message = "Dus jij wilt je wachtwoord veranderen? \r\n Je nieuwe wachtwoord is ". $newpass .". \r\n ".$url;
        
        $headers = 'From: ProjectW@uu.nl' . "\r\n" .
            'Reply-To: noreply.projectw@gmail.com' . "\r\n" .
            'Content-type: text/html; charset=iso-8859-1' . "\r\n".
            'X-Mailer: PHP/' . phpversion();
        // Send
        mail($mail, 'Wachtwoord reset', $message, $headers);
    }
    
    public function verzendActivatiemail($mail, $voornaam)
    {
        foreach($this->database_model->getUserbyMail($mail) as $row)
        {
            $UserID = $row['UserID'];
        }
        
        $url = "<html><a href='https://www.projects.science.uu.nl/INFOB1PICA/2015/02/www/index.php/Login/activeerAccount/$UserID'>Klik hier om je account te activeren.</a></html>";
     
        $message = "
        
        Hallo $voornaam,                                                                                                                             $url
        ";

        $headers = 'From: ProjectW@uu.nl' . "\r\n" .
            'Reply-To: noreply.projectw@gmail.com' . "\r\n" .
            'Content-type: text/html; charset=iso-8859-1' . "\r\n".
            'X-Mailer: PHP/' . phpversion();
        mail($mail, 'Activeer je account', $message, $headers);
    }
    
    public function activeerAccount($UserID)
    {   
        $this->database_model->insertActief($UserID);
        redirect('index.php/Login');       
    }
    
    public function nieuwAcc() 
    {
        $this->load->model('database_model');
        
        // Kijken of de velden in orde zijn
        if (empty($this->input->post('email'))) 
        {
            $_SESSION["emailErr"] = "Vul je emailadres in";
            redirect('index.php/Login/registratie');
            return;
        } 
        else 
        {
            $email = $this->input->post('email');
            $_SESSION["emailErr"] = "";
            $_SESSION["email"] = "$email";

            foreach ($this->database_model->getEmail($email) as $row) 
            {
                $result = $row['Email'];

                if ($result === $email)
                {
                    $_SESSION["emailErr"] = "Dit adres is al in gebruik";
                    redirect('index.php/Login/registratie');
                    return;
                }
            }
        }
        
        if (empty($this->input->post('password'))) 
        {
            $_SESSION["passwErr"] = "Er is een wachtwoord nodig";
            redirect('index.php/Login/registratie');
            return;
        } 
        else if (strlen($this->input->post('password'))<7) 
        {
            $_SESSION["passwErr"] = "Wachtwoord moet minimaal 7 tekens bevatten."; 
            redirect('index.php/Login/registratie');
            return;
        } 
        else if(($this->input->post('password')) != ($this->input->post('password2')))
        {
            $_SESSION["passwcheckErr"] = "Uw wachtwoorden komen niet overeen."; 
            redirect('index.php/Login/registratie');
            return;
        }
        else 
        {
            $pass = $this->input->post('password');
            $_SESSION["passwErr"] = "";
        }

        if (empty($this->input->post('voornaam'))) 
        {   
            $_SESSION["vnaamErr"] = "Je heb vast wel een naam...";
            redirect('index.php/Login/registratie');
            return;
        } 
        else 
        {
            $voornaam = $this->input->post('voornaam');
            $_SESSION["vnaamErr"] = "";
            $_SESSION["vnaam"] = "$voornaam";
        }

        if (empty($this->input->post('achternaam'))) 
        {
            $_SESSION["anaamErr"] = "Je heb vast wel een naam...";
            redirect('index.php/Login/registratie');
            return;
        } 
        else 
        {
            $achternaam = $this->input->post('achternaam');
            $_SESSION["anaamErr"] = "";
            $_SESSION["anaam"] = "$achternaam";
        }
        
        $gebdate = date('Y-m-d', strtotime($this->input->post('geboortedatum')));
        $pass = password_hash($pass, PASSWORD_DEFAULT);
        $geslacht = $this->input->post('geslacht');
        $this->database_model->insertUser($email, $pass, $gebdate, $voornaam, $achternaam, $geslacht);
        $this->verzendActivatiemail($email, $voornaam, $pass);
        $this->_resetRegisterVar();
        redirect('index.php/Login');
    }

    // Initialiseert de variabelen voor de view
    private function _initLoginVar(&$data) 
    {
        $this->projectw->_initVar($data, 'email');
        $this->projectw->_initVar($data, 'emailErr');
        $this->projectw->_initVar($data, 'passErr');
        $this->projectw->_initVar($data, 'passwErr');
        $this->projectw->_initVar($data, 'passwcheckErr');
        $this->projectw->_initVar($data, 'vnaam');
        $this->projectw->_initVar($data, 'vnaamErr');
        $this->projectw->_initVar($data, 'anaam');
        $this->projectw->_initVar($data, 'anaamErr');
        $this->projectw->_initVar($data, 'activatieErr');
    }
    
    private function _initPassChangeVar(&$data)
    {
        $this->projectw->_initVar($data, 'passErr');
        $this->projectw->_initVar($data, 'passErr2');
    }

    private function _resetRegisterVar()
    {
        $this->projectw->_resetVar('email');
        $this->projectw->_resetVar('emailErr');
        $this->projectw->_resetVar('passErr');
        $this->projectw->_resetVar('passwErr');
        $this->projectw->_resetVar('passwcheckErr');
        $this->projectw->_resetVar('vnaam');
        $this->projectw->_resetVar('vnaamErr');
        $this->projectw->_resetVar('anaam');
        $this->projectw->_resetVar('anaamErr');
    }
}