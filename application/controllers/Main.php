<?php

class Main extends MY_Controller {
    
    // Page die verschijnt als er niets specifieks is in de URL
    // De andere gaan naar de verschillende pagina's van de menubalk
    public function index(){
        // De title van een page wordt doorgegeven via de $data array
        // Hij is dan verkrijgbaar als $title in header.php
        // De verschillende views worden zo samengesteld tot een page
        $data['title'] = "Home";
        $this->load->view('header.php', $data);
        $this->load->view('menubalk.php');
        $this->load->view('home.php');
        $this->load->view('footer.php');
    }
    
    public function events(){
        $data['title'] = "Events";
        $this->load->view('header.php', $data);
        $this->load->view('menubalk.php');
        $this->load->view('events.php');
        $this->load->view('footer.php');    
    }
	
	public function review()
    {
        $data['title'] = "Events";
        $this->load->view('header.php', $data);
        $this->load->view('menubalk.php');
        $this->load->view('review.php');
        $this->load->view('footer.php');    
    }
	
	public function all_events(){
        redirect('index.php/Openevents');
    }
    
    public function info(){
        $data['title'] = "Info";
        $this->load->view('header.php', $data);
        $this->load->view('menubalk.php');
        $this->load->view('Info.php');
        $this->load->view('footer.php');    
    }
    
    public function Copyright(){
        $data['title'] = "Copyright";
        $this->load->view('header.php', $data);
        $this->load->view('menubalk.php');
        $this->load->view('copyright.php');
        $this->load->view('footer.php');  
    }
	
	public function employment(){
        $data['title'] = "Werken bij Project W";
        $this->load->view('header.php', $data);
        $this->load->view('menubalk.php');
        $this->load->view('employment.php');
        $this->load->view('footer.php');    
    }
	
	public function birthday(){
        $data['title'] = "Project W: 3 maanden oud";
        $this->load->view('header.php', $data);
        $this->load->view('menubalk.php');
        $this->load->view('birthday.php');
        $this->load->view('footer.php');    
    }
}
?>