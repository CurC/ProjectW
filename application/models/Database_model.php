<?php

class Database_model extends CI_Model {

    public function __construct()
    {
        parent::__construct();
    }

    /*
    |------------------------------------------------------------------------
    | Users
    |------------------------------------------------------------------------
    */
    public function getUser($user)
    {
        $sql = "SELECT * FROM Users WHERE Email = ?";
        $query = $this->db->query($sql, array($user));
        return $query->result_array();
    }
    public function getUserByID($user)
    {
        $sql = "SELECT * FROM Users WHERE UserID = ?";
        $query = $this->db->query($sql, array($user));
        return $query->result_array();
    }
    public function getUserbyEvent($EventID)
    {
        $sql = "SELECT * FROM EventsUser WHERE EventID = ?";
        $query = $this->db->query($sql, array($EventID));
        return $query->result_array();
    }
    public function getEmptyGastID()
    {
        $sql = "SELECT Email FROM Users WHERE GastID = '0'";
        $query = $this->db->query($sql);
        return $query->result_array();
    }

    public function setGastID($mail, $gastID)
    {
        $sql="UPDATE Users SET GastID = ? WHERE Email = ?";
        $this->db->query($sql, array($gastID, $mail));
    }
    
    
    public function resetpass($mail, $newpasshash)
    {
        $sql="UPDATE Users SET PassHash = ? WHERE Email = ?";
        $this->db->query($sql, array($newpasshash, $mail));
    }

    public function changepass($UserID, $newpasshash)
    {
        $sql="UPDATE Users SET PassHash = ? WHERE UserID = ?";
        $this->db->query($sql, array($newpasshash, $UserID));
    }

    public function insertUser($email, $pass, $gebdate, $voornaam, $achternaam, $geslacht)
    {
		$sql = "INSERT INTO Users (Email, PassHash, GeboorteDatum, Voornaam, Achternaam, Geslacht) VALUES ( ?, ?, ?, ?, ?, ?)";
        $this->db->query($sql, array($email, $pass, $gebdate, $voornaam, $achternaam, $geslacht));
    }

    public function getEmail($email)
    {
        $sql = "SELECT Email FROM Users WHERE Email = ?";
        $query = $this->db->query($sql, array($email));
        return $query->result_array();
    }

    public function getNameHost($HostID)
    {
        $sql = "SELECT Voornaam, Achternaam, Email FROM Users WHERE UserID = ?";
        $query = $this->db->query($sql, array($HostID));
        return $query->result_array();
    }

    public function updateCookieData($UserID, $IPHash)
    {
        $sql = "UPDATE Users SET IPHash = ? WHERE UserID = ?";
        $this->db->query($sql, array($IPHash, $UserID));
    }

    public function getUserIPHash($UserID)
    {
        $sql = "SELECT IPHash FROM Users WHERE UserID = ?";
        $query = $this->db->query($sql, array($UserID));
        return $query->result_array();
    }
    
    public function getUserbyMail($mail)
    {
        $sql = "SELECT UserID FROM Users WHERE Email = ?";
        $query = $this->db->query($sql, array($mail));
        return $query->result_array();
    }
    
    public function insertActief($UserID)
    {
        $sql = "UPDATE Users SET Actief = '1' WHERE UserID = ?";
        $this->db->query($sql, array($UserID));
    }
    
    /*
    |------------------------------------------------------------------------
    | Events
    |------------------------------------------------------------------------
    */
    public function insertEvent($EventID)
    {
        $sql = "INSERT INTO Events(EventID, DatumBool, LocatieBool, Gastenbool, Kostenbool) VALUES(?, '0', '0', '0', '0' )";
        $this->db->query($sql, array($EventID));
    }

    public function getEvent($EventID)
    {
        $sql = "SELECT * FROM Events WHERE EventID = ?";
        $query = $this->db->query($sql, array($EventID));
        return $query->result_array();
    }

    public function removeEvent($EventID)
    {
        $sql = "DELETE FROM Events WHERE EventID = '$EventID'";
        $this->db->query($sql);
    }

    public function updateEventBool($EventID, $BoolID)
    {
        if ($_SESSION["$BoolID"] == 0)
        {
            $sql = "UPDATE Events SET $BoolID= '1' WHERE EventID = ?";
        } else {
            $sql = "UPDATE Events SET $BoolID= '0' WHERE EventID = ?";
        }
        $this->db->query($sql, array($EventID));
    }

    public function updateGuestVoltooid($EventID)
    {
        $sql = "UPDATE Events SET GastenVoltooid = '1' WHERE EventID = ?";
        $this->db->query($sql, array($EventID));
    }
	
	public function insertBeschrijving($EventID, $beschrijving_tekst)
    {
        $sql = "UPDATE Events SET Beschrijving = ? WHERE EventID = ?";
        $this->db->query($sql, array($beschrijving_tekst, $EventID));
    }

    public function getBeschrijving($EventID)
    {    
        $sql = "SELECT Beschrijving FROM Events WHERE EventID = ?";
        $query = $this->db->query($sql, array($EventID));
        return $query->result_array();
    }
    
    public function updateCostVoltooid($EventID)
    {
        $sql = "UPDATE Events SET KostenVoltooid = '1' WHERE EventID = ?";
        $this->db->query($sql, array($EventID));
    }
	
	public function getBool($EventID)
    {
        $sql = "SELECT DatumBool, LocatieBool, Gastenbool, Kostenbool FROM Events WHERE EventID = ?";
        $query = $this->db->query($sql, array($EventID));
        return $query->result_array();
    }
    
    public function setOpenEvent($EventID, $OEvent)
    {
        $sql = "UPDATE Events SET OpenEvenement = ? WHERE EventID = ?";
        $this->db->query($sql, array($OEvent, $EventID));
    }
    
    public function getOpenEvents()
    {
        $sql = "SELECT * FROM Events WHERE OpenEvenement = '1'";
        $query = $this->db->query($sql);
        return $query->result_array();
    }
    
    /*
    |------------------------------------------------------------------------
    | EventsUser
    |------------------------------------------------------------------------
    */
    public function insertEventsUser($eventname, $userid)
    {
        $sql = "INSERT INTO EventsUser(EventName, UserID) VALUES(?, ?)";
        $this->db->query($sql, array($eventname, $userid));
    }
    
    public function getEventsUser($eventname, $userid)
    {
        $sql = "SELECT EventID FROM EventsUser WHERE UserID = ? AND EventName = ?";
        $query = $this->db->query($sql, array($userid, $eventname));
        return $query->result_array();
    }
    
    public function getEventNameHost($levent)
    {
        $sql = "SELECT EventName, UserID, Deleted FROM EventsUser WHERE EventID = ?";
        $query = $this->db->query($sql, array($levent));
        return $query->result_array();
    }
     public function getEventNameNonDeleted($EventID)
    {
        $sql = "SELECT * FROM EventsUser WHERE EventID = ? AND NOT Deleted = '1'";
        $query = $this->db->query($sql, array($EventID));
        return $query->result_array();
    }

    public function getEventByUserID($UserID)
    {
        $sql = "SELECT * FROM EventsUser WHERE UserID = ? AND NOT Deleted = '1'";
        $query = $this->db->query($sql, array($UserID));
        return $query->result_array();
    }

    public function removeEventsUser($EventID)
    {
        $sql = "DELETE FROM EventsUser WHERE EventID = ?";
        $this->db->query($sql, array($EventID));
    }

    public function setDeleted($EventID)
    {
        $sql = "Update EventsUser SET Deleted = '1' WHERE EventID = ?";
        $this->db->query($sql, array($EventID));
    }
    
    /*
    |------------------------------------------------------------------------
    | Datumprikker
    |------------------------------------------------------------------------
    */
    public function getDataByEventID($EventID)
    {
        $sql = "SELECT Data1, Data2, Data3, Data4 FROM Data WHERE EventID = ?";
        $query = $this->db->query($sql, array($EventID));
        return $query->result_array();
    }

    public function insertIntoData($EventID, $Data, $num)
    {
        $sql1 = "SELECT EventID FROM Data WHERE EventID = ?";
        $query = $this->db->query($sql1, array($EventID));

        if ($query->num_rows() == 0)
        {
            $sql = "INSERT INTO Data(EventID, Data1) VALUES(?, ?)";
            $this->db->query($sql, array($EventID, $Data));
        }
        else
        {
            switch ($num)
            {
                case 1:
                    $sql = "UPDATE Data SET Data1 = ? WHERE EventID = ?";
                    break;
                case 2:
                    $sql = "UPDATE Data SET Data2 = ? WHERE EventID = ?";
                    break;
                case 3:
                    $sql = "UPDATE Data SET Data3 = ? WHERE EventID = ?";
                    break;
                case 4:
                    $sql = "UPDATE Data SET Data4 = ? WHERE EventID = ?";
                break;
            }
            $this->db->query($sql, array($Data, $EventID));
        }
    }

    
    
    public function setFinal($EventID, $num){
                // zet de data op final
            switch ($num){
                    
                case 1:
                    $sql = "UPDATE Data SET Data1Final = '1' ,
                     Data2Final = '0',
                     Data3Final = '0',
                     Data4Final = '0'
                    WHERE EventID = ?";
                    break;
                case 2:
                    $sql = "UPDATE Data SET Data1Final = '1' ,
                     Data2Final = '1',
                     Data3Final = '0',
                     Data4Final = '0'
                    WHERE EventID = ?";
                    break;
                case 3:
                    $sql = "UPDATE Data SET Data1Final = '1' ,
                     Data2Final = '0',
                     Data3Final = '1',
                     Data4Final = '0'
                    WHERE EventID = ?";
                    break;
                case 4:
                    $sql = "UPDATE Data SET Data1Final = '1' ,
                     Data2Final = '0',
                     Data3Final = '0',
                     Data4Final = '1'
                    WHERE EventID = ?";
                break;        
           
        
    }
    
    $this->db->query($sql, array($EventID));
    }
    
    
    public function removeData($EventID)
    {
        $sql = "UPDATE Data SET Data1 = '0', Data2 ='0', Data3 = '0', Data4 = '0' WHERE EventID = ?";
        $this->db->query($sql, array($EventID));
    }
	
	public function checkData($EventID)
    {
        $sql = "SELECT EventID FROM Data WHERE EventID = ?";
		$query = $this->db->query($sql, array($EventID));
		if ($query->num_rows() == 0)
		{
			return false;
		}
		else
		{
			return true;
		}
	}
	
	public function voltooiDate($EventID)
    {
		$sql = "UPDATE Events Set DatumVoltooid = '1' WHERE EventID = ?";
		$this->db->query($sql, array($EventID));
	}

    /*
    |------------------------------------------------------------------------
    | Locatie
    |------------------------------------------------------------------------
    */
    public function insertLocation($Loc, $EventID, $Coor)
    {
        $sql = "UPDATE Events SET Locatie= ?, LocatieVoltooid= '1', Coordinaten= ? WHERE EventID=?";
        $this->db->query($sql, array($Loc, $Coor, $EventID));
    }
    
    public function getLocation($EventID)
    {
        $sql = "SELECT Locatie FROM Events WHERE EventID = ?";
        $query = $this->db->query($sql, array($EventID));
        return $query->result_array();
    }
	
	public function getCoords($EventID)
	{
		$sql = "SELECT Coordinaten From Events WHERE EventID = ?";
		$query = $this->db->query($sql, array($EventID));
		return $query->result_array();
	}

    /*
    |------------------------------------------------------------------------
    | Gasten
    |------------------------------------------------------------------------
    */
    public function getGastID($EventID)
    {
        $sql = "SELECT GastID, GastNaam, NieuwMail FROM GastEvents WHERE EventID = ?";
        $query = $this->db->query($sql, array($EventID));
        return $query->result_array();
    }

    public function getgastmail($gastid)
    {
        $sql = "SELECT GastMail, GastNaam FROM Gasten WHERE GastID = ?";
        $query = $this->db->query($sql, array($gastid));
        return $query->result_array();
    }

    public function getGEmail($gemail)
    {
        $sql = "SELECT GastMail, GastID FROM Gasten WHERE GastMail = ?";
        $query = $this->db->query($sql, array($gemail));
        return $query->result_array();
    }

    public function getVoltooidKnoppen($EventID)
    {
        $sql = "SELECT GastenVoltooid, DatumVoltooid, LocatieVoltooid, KostenVoltooid FROM Events WHERE EventID = ?";
        $query = $this->db->query($sql, array($EventID));
        return $query->result_array();
    }

    public function getGuestByEmail($GastMail)
    {
        $sql = "SELECT GastID, GastNaam, GastMail FROM Gasten WHERE GastMail =  ?";
        $query = $this->db->query($sql, array($GastMail));
        return $query->result_array();
    }

    public function getGuestByID($GastID)
    {
        $sql = "SELECT GastID, GastNaam, GastMail FROM Gasten WHERE GastID= ?";
        $query = $this->db->query($sql, array($GastID));
        return $query->result_array();
    }

    public function insertGuest($gnaam, $gemail)
    {
        $sql = "INSERT INTO Gasten (GastNaam, GastMail) VALUES (?, ?)";
        $this->db->query($sql, array($gnaam, $gemail));
    }

    /*
    |------------------------------------------------------------------------
    | GastEvents
    |------------------------------------------------------------------------
    */
    public function getGuestEvent($EventID)
    {
        $sql = "SELECT * FROM GastEvents WHERE EventID = ?";
        $query = $this->db->query($sql, array($EventID));
        return $query->result_array();
    }
    
    public function getEventIDByGastID($gastID)
    {
        $sql = "SELECT Aanwezig, GastNaam, EventID FROM GastEvents WHERE GastID = ?";
        $query = $this->db->query($sql, array($gastID));
        return $query->result_array();
    }

    public function getGuestEventByGastIDAndEventID($GastID, $EventID)
    {
        $sql = "SELECT * FROM GastEvents WHERE GastID = ? AND EventID = ?";
        $query = $this->db->query($sql, array($GastID, $EventID));
        return $query->result_array();
    }

    public function insertGuestEvent($EventID, $GastID, $gastnaam)
    {
        $sql = "INSERT INTO GastEvents (EventID, GastID, GastNaam) VALUES (?, ?, ?)";
        $this->db->query($sql, array($EventID, $GastID, $gastnaam));
    }

    public function removeGuestEvent($EventID, $gastID)
    {
        $sql = "DELETE FROM GastEvents WHERE GastID = ? AND EventID = ?";
        $this->db->query($sql, array($gastID, $EventID));
    }
    
    public function setNieuwMail($gastid, $eventid, $tomail)
    {
        $sql = "UPDATE GastEvents SET NieuwMail = ? WHERE EventID = ? AND GastID = ?";
        $this->db->query($sql, array($tomail, $eventid, $gastid));
    }
    
    public function setAanwezig($gastid, $eventid)
    {
        $sql = "UPDATE GastEvents SET Aanwezig = '1' WHERE EventID = ? AND GastID = ?";
        $this->db->query($sql, array($eventid, $gastid));
    }

    public function setAfwezig($gastid, $eventid)
    {
        $sql = "UPDATE GastEvents SET Aanwezig = '2' WHERE EventID = ? AND GastID = ?";
        $this->db->query($sql, array($eventid, $gastid));
    }
    
    public function setD1J($gastid, $eventid){
        $sql = "UPDATE GastEvents SET Datum1 = '1' WHERE EventID = ? AND GastID = ?";
        $this->db->query($sql, array($eventid, $gastid));
    }
    public function setD1N($gastid, $eventid){
        $sql = "UPDATE GastEvents SET Datum1 = '0' WHERE EventID = ? AND GastID = ?";
        $this->db->query($sql, array($eventid, $gastid));
    }
    
     public function setD2J($gastid, $eventid){
        $sql = "UPDATE GastEvents SET Datum2 = '1' WHERE EventID = ? AND GastID = ?";
        $this->db->query($sql, array($eventid, $gastid));
    }
    public function setD2N($gastid, $eventid){
        $sql = "UPDATE GastEvents SET Datum2 = '0' WHERE EventID = ? AND GastID = ?";
        $this->db->query($sql, array($eventid, $gastid));
    }
    
     public function setD3J($gastid, $eventid){
        $sql = "UPDATE GastEvents SET Datum3 = '1' WHERE EventID = ? AND GastID = ?";
        $this->db->query($sql, array($eventid, $gastid));
    }
    public function setD3N($gastid, $eventid){
        $sql = "UPDATE GastEvents SET Datum3 = '0' WHERE EventID = ? AND GastID = ?";
        $this->db->query($sql, array($eventid, $gastid));
    }
    
     public function setD4J($gastid, $eventid){
        $sql = "UPDATE GastEvents SET Datum4 = '1' WHERE EventID = ? AND GastID = ?";
        $this->db->query($sql, array($eventid, $gastid));
    }
    public function setD4N($gastid, $eventid){
        $sql = "UPDATE GastEvents SET Datum4 = '0' WHERE EventID = ? AND GastID = ?";
        $this->db->query($sql, array($eventid, $gastid));
    }
    
	/*
    |------------------------------------------------------------------------
    | Kosten
    |------------------------------------------------------------------------
    */
	
	public function getCost($EventID)
    {
        $sql = "SELECT * FROM Kosten WHERE EventID = ?";
        $query = $this->db->query($sql, array($EventID));
        return $query->result_array();
    }
	
    public function removeCost($EventID, $KostenID)
    {
        $sql = "DELETE FROM Kosten WHERE EventID = ? AND KostenID = ?";
        $this->db->query($sql, array($EventID, $KostenID));
    }

    public function insertCost($KostNaam, $KostPrijs, $EventID)
    {
        $sql = "INSERT INTO Kosten (KostNaam, KostPrijs, EventID) VALUES (?, ?, ?)";
        $this->db->query($sql, array($KostNaam, $KostPrijs, $EventID));
    }
}